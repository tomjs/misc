// no-target-omega - Reset target omega rotation
// self-deleting

default {
    state_entry() {
        // Reset target omega
        llTargetOmega(ZERO_VECTOR, 0, 0);

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
