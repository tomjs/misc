Set and Delete
==============

Quick scripts to set or reset prim properties.

desc2float - Copy object description to floating text; self-deleting script

name2float.lsl - Copy object name to floating text; self-deleting script

no-*.lsl - A set of scripts that turn off various prim properties and delete itself
