// multi sit prim using Json Arrays (expects 6 seats)
// written by To-mos Codewarrior(tomos.halsey)
// http://wiki.secondlife.com/wiki/Json_multi_sit_array
//
// v2 - Make seat count more configurable

// How many sit positions
integer NUM_SEATS = 3;

// Sit positions
// Note: twist is in degrees on the Z axis
list seat0 = ["key", "empty", "pos", <0.00000, -0.32011, 0.65>, "twist", 270.0];
list seat1 = ["key", "empty", "pos", <-0.65000, -0.32011, 0.65>, "twist", 270.0];
list seat2 = ["key", "empty", "pos" ,<0.65000, -0.32011, 0.65>, "twist", 270.0];

vector DEFAULT_SIT_POS = <0.0, 0.0, 0.1>;
rotation DEFAULT_SIT_ROTATION = ZERO_ROTATION;

// Log debug output?
integer VERBOSE = TRUE;

string SEATS_data;

log(string msg) {
    if (VERBOSE && msg != "") {
        llOwnerSay(msg);
    }
}

list getFreeSeat() {
    string avi;
    integer i = llGetNumberOfPrims();
    list avatarCheck;
    list sittingAvis;
    integer listIndex;
    string newAvi;
    integer newAviFlag=FALSE;

    //get current avatars on seat
    for (;i--;) {
        if (llGetAgentSize(llGetLinkKey(i+1)) != ZERO_VECTOR) {
            avatarCheck += [llGetLinkKey(i+1)];
        }
    }
    log("Current Avatars: " + llList2CSV(avatarCheck));
    //run garbage cleaning loop to dump dead keys
    //and store the existing ones for checking
    for (i=NUM_SEATS; i--;) {
        avi = llJsonGetValue(SEATS_data, ["seat"+(string)i, "key"]);
        if (avi != "empty") {
            //if avi isn't on the list set it to empty
            listIndex = llListFindList(avatarCheck, [(key)avi]);
            if (!~listIndex)
                SEATS_data = llJsonSetValue(SEATS_data, ["seat"+(string)i, "key"], "empty");
            else
                sittingAvis += avi;
        }
    }
    //identify the new key
    i = llGetListLength(avatarCheck);
    for (;i--;) {
        listIndex = llListFindList(sittingAvis, [llList2String(avatarCheck, i)]);
        if ((!~listIndex) && !newAviFlag) {
            newAviFlag = TRUE;
            newAvi = llList2String(avatarCheck, i);
            log("The new avatar is: " + newAvi);
        }
    }
    //check their numbers
    if (llGetListLength(avatarCheck) > NUM_SEATS) {
        if (newAvi != "")
            llSay(0, "Too many avatars are on me, please get off " + llKey2Name(newAvi));
        else
            llSay(0, "More avatars need to get off me.");
        return ["full"];
    }
    //no need to go further after cleaning
    //up the array and have empty newAvi
    if (newAvi == "")
        return ["none"];
    log("New avatar is: " + (string)newAvi);
    //now find first instance of empty to dump the new key
    //reset the new avi flag for the next loop
    newAviFlag = FALSE;
    for (i=NUM_SEATS; i--;) {
        avi = llJsonGetValue(SEATS_data,["seat"+(string)i, "key"]);
        if (avi == "empty" && !newAviFlag) {
            SEATS_data = llJsonSetValue(SEATS_data, ["seat"+(string)i, "key"], (string)newAvi);
            //just use avatarCheck list as a temp variable for the output
            avatarCheck = [llJsonGetValue(SEATS_data, ["seat"+(string)i,"pos"]), llJsonGetValue(SEATS_data, ["seat"+(string)i, "twist"])];
            newAviFlag = TRUE;
        }
    }
    return avatarCheck;
}

default {
    state_entry() {
        SEATS_data = llList2Json(
            JSON_OBJECT,
            [
                "seat0", llList2Json(JSON_OBJECT, seat0),
                "seat1", llList2Json(JSON_OBJECT, seat1),
                "seat2", llList2Json(JSON_OBJECT, seat2)
            ]
        );
        llSetClickAction(CLICK_ACTION_SIT);
        llSitTarget(DEFAULT_SIT_POS, DEFAULT_SIT_ROTATION);
    }

    changed(integer change) {
        if (change & CHANGED_LINK) {
            list output = getFreeSeat();
            if (output != ["full"] && output != ["none"])
                llSetLinkPrimitiveParamsFast(
                    llGetNumberOfPrims(),
                    [
                        PRIM_POS_LOCAL, (vector)llList2String(output, 0),
                        PRIM_ROT_LOCAL, llEuler2Rot(<0.0, 0.0, llList2Float(output, 1) * DEG_TO_RAD>)
                    ]
                );

            // Debug draw array
            log(SEATS_data);
        }
    }
}
