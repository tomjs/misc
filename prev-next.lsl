// prev_next - Cycle through textures
//
// Click to cycle through textures in inventory, click on the left side
// to go back and on the right side to go forward.

// Set to image advance time, 0 for no auto-advance
float time = 0;

// Adjust this to the correct face or use ALL_SIDES
integer face = 3;

// Display float text
integer SHOW_FLOAT = FALSE;

integer total;
integer counter = -1;

next() {
    total=llGetInventoryNumber(INVENTORY_TEXTURE);
    counter++;
    if(counter >= total) {
        counter=0;
    }
    llSetTexture(llGetInventoryName(INVENTORY_TEXTURE, counter),face);
    SetLinkText(LINK_ROOT, llGetInventoryName(INVENTORY_TEXTURE, counter), <1,1,1>, 1);
    llTriggerSound("Pressed", 1);
}

prev() {
    total=llGetInventoryNumber(INVENTORY_TEXTURE);
    if (counter <= 0) {
        counter=total-1;
    } else {
        counter--;
    }
    llSetTexture(llGetInventoryName(INVENTORY_TEXTURE, counter),face);
    SetLinkText(LINK_ROOT, llGetInventoryName(INVENTORY_TEXTURE, counter), <1,1,1>, 1);
    llTriggerSound("Pressed", 1);
}

SetLinkText(integer linknum, string text, vector color, float alpha) {
    if (SHOW_FLOAT) {
        llSetLinkPrimitiveParamsFast(linknum, [PRIM_TEXT, text, color, alpha]);
    }
}

default {
    state_entry() {
        next();
        llSetTimerEvent(time);
    }

    touch_start(integer total_number) {
        vector p = llDetectedTouchST(0);
//        llOwnerSay("p: " + (string)p + ", counter: " + (string)counter);
        if (p.x < .5) {
            prev();
        } else {
            next();
        }
    }

    timer() {
        next();
    }

    on_rez(integer start_param) {
        llResetScript();
    }
}
