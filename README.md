Misc
====

animate.lsl - Set texture animation on one or more prim faces; self-deleting script

book.lsl - A simple two-page texture display that acts as a book HUD

book-page.lsl - The texture display slave for book.lsl

cache-animations.lsl - Simple animation cache that plays all animations in inventory when sat on

curtain-control.lsl - Controller for a stage curtain that an open/close a prim like a regular curtain side to side or up and down.  It also has a fade in/out mode.

FixSmallPrims - Slightly increases the size of small prim dimensions (close to 0.01m)
to allow shrinking

hold_item.lsl - Play an animation when an object is attached/worn

inventory-giver - variation on the many inventory copy scripts

kf-animation.lsl - Basic key frame animator

lightning_control.lsl - Chat-triggered lightning flashes

linkset-resizer.lsl - What it says, menu-driven resizing (Brilliant Scientist/Sei Lisa)

linkset-resizer-2.lsl - What it says, menu-driven resizing (Emma Nowhere)

multi-sit-json.lsl - Permit multiple avatars to sit on an object (default 3)

proximite-sensor.lsl - Change prim when target avatar is in range, changes alpha and/or light

resizer.lsl - Shrink/enlarge linksets by a percentage

sit-linkset.lsl - Sets up a sit target in each prim of a linkset and starts the first animation in object inventory when an avatar sits on a link

slideshow.lsl - Display textures from object inventory
