// no-keyframe - Reset key frame animation
// self-deleting

default {
    state_entry() {
        // Reset key frame properties
        llSetKeyframedMotion([], []);

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
