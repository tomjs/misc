// lightning control - Click to toggle visibility
// v1 - Initial release
// v2 - Handle multiple faces, add glow
// v3 - add timed delay

// * Set CHANNEL to a positive number to listen for commands in local chat
// * The 'flash' command can take an optional number of seconds to wait before
//   triggering the flash; useful to trigger a delayed flash from a HUD.

float delay = 0.0;
float fudge = 0.0;
float on1 =   0.02;
float wait =  0.05;
float on2 =   0.03;
float on3 =   0.04;

// FACE_OUTSIDE and FACE_INSIDE refer to the vertical faces of a hollow cylinder
// to show two planes of lightning, but can be any faces of a prim depending on need.
integer FACE_OUTSIDE = 1;
integer FACE_INSIDE = 2;

// Hide on rez
integer HIDE_ON_REZ = TRUE;

// Listen channel
integer CHANNEL = 30;

// Spew debug info
integer VERBOSE = FALSE;

integer visible;

log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

hide() {
    llSetLinkPrimitiveParams(LINK_THIS, [
        PRIM_COLOR, ALL_SIDES, <1,1,1>, 0.0,
        PRIM_GLOW, ALL_SIDES, 0.0
    ]);
    visible = FALSE;
}

show() {
    llSetLinkAlpha(LINK_THIS, 1.0 ,ALL_SIDES);
    visible = TRUE;
}

toggle() {
    if (visible) {
        hide();
    } else {
        show();
    }
}

flash() {
    llSleep(delay);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [
        PRIM_COLOR, FACE_OUTSIDE, <1,1,1>, 1.0,
        PRIM_GLOW, FACE_OUTSIDE, 0.01
    ]);
    llSleep(on1);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [
        PRIM_COLOR, FACE_OUTSIDE, <1,1,1>, 0.0,
        PRIM_GLOW, FACE_OUTSIDE, 0.0,
        PRIM_COLOR, FACE_INSIDE, <1,1,1>, 1.0,
        PRIM_GLOW, FACE_INSIDE, 0.01
    ]);
    llSleep(on2);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [
        PRIM_COLOR, FACE_INSIDE, <1,1,1>, 0.0,
        PRIM_GLOW, FACE_INSIDE, 0.0
    ]);

    llSleep(0.1);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [
        PRIM_COLOR, FACE_OUTSIDE, <1,1,1>, 1.0,
        PRIM_GLOW, FACE_OUTSIDE, 0.01
    ]);
    llSleep(on3);
    llSetLinkPrimitiveParamsFast(LINK_THIS, [
        PRIM_COLOR, FACE_OUTSIDE, <1,1,1>, 0.0,
        PRIM_GLOW, FACE_OUTSIDE, 0.0
    ]);
}

diddle() {
    delay = llFrand(0.4);
    fudge = llFrand(0.2);
}

reset() {
    diddle();
    if (HIDE_ON_REZ) {
        hide();
    } else {
        show();
    }
}

default {
    state_entry() {
        reset();

        llListen(CHANNEL, "", NULL_KEY, "");
        log("listening on " + (string)CHANNEL);
    }

    listen(integer channel, string name, key id, string message) {
        // Parse command
        string cmd = "";
        string args = "";
        integer idx = llSubStringIndex(message, " ");
        if (idx == -1) {
            cmd = message;
            args = "";
        } else {
            cmd = llGetSubString(message, 0, idx-1);
            args = llDeleteSubString(message, 0, idx);
        }
        cmd = llToLower(cmd);
        log("cmd: " + cmd);

        // Parse a number arg
        float num = (float)args;

        if (cmd == "reset") {
            reset();
        } else if (cmd == "flash") {
            if (num > 0.0) {
                llSetTimerEvent(num);
            } else {
                flash();
            }
        } else if (cmd == "hide") {
            hide();
        } else if (cmd == "show") {
            show();
        }
    }

    timer() {
        llSetTimerEvent(0.0);
        flash();
    }
}
