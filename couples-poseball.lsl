// Pose Balls sync script by Dora Gustafson, Studio Dora 2013
// Free for anybody to read, copy, modify, compile, use, rip apart, trample on and flush
// Build using Basic pose ball script v1.4
// Will synchronize animations in two or more pose balls
// All animations start when all are seated
// Put the script and some animation in each pose ball
// All pose balls must be linked
// When one or more sitters leave all animations stop

// From http://wiki.secondlife.com/wiki/User:Toady_Nakamura/Couples_Poseball


string animation;
key subject;

default {
    state_entry() {
        llSitTarget(<0.0, 0.0, 0.01>, ZERO_ROTATION);
        llSetClickAction(CLICK_ACTION_SIT);
        animation = llGetInventoryName(INVENTORY_ANIMATION, 0);
        if (animation == "")
            animation = "sit_generic";
    }

    changed(integer change) {
        if (change & CHANGED_LINK) {
            key sitter = llAvatarOnSitTarget() ;
            if (sitter) {
                llSetAlpha(0.0, ALL_SIDES); // hide prim
                subject = sitter;
            } else {
                if (llGetPermissions() & PERMISSION_TRIGGER_ANIMATION && llGetAgentSize(subject) != ZERO_VECTOR)
                    llStopAnimation(animation);
                llSetAlpha(1.0, ALL_SIDES); // show prim
            }
            if ((llGetNumberOfPrims() / llGetObjectPrimCount(llGetKey())) == 2)
                llRequestPermissions(sitter , PERMISSION_TRIGGER_ANIMATION);
            else if (llGetPermissions() & PERMISSION_TRIGGER_ANIMATION && llGetAgentSize(subject) != ZERO_VECTOR) {
                llStartAnimation("express_disdain");
                llStopAnimation(animation);
            }
        }
        if (change & CHANGED_INVENTORY)
            llResetScript();
    }

    run_time_permissions(integer perm) {
        if (perm & PERMISSION_TRIGGER_ANIMATION) {
            llStartAnimation(animation);
            llStopAnimation("sit");
        }
    }

    on_rez(integer param) {
        llResetScript();
    }
}
