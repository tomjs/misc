// Animate textures
// self-deleting

integer face = ALL_SIDES;
float rate = 0.02;

// Default animation style
integer mode = ANIM_ON;

// Optionally pick one of the below too

// slide in X direction
//mode = ANIM_ON | LOOP | SMOOTH;

// rotate
//mode = ANIM_ON | LOOP | SMOOTH | ROTATE;

// stop animation
//mode = FALSE;

default {

    state_entry() {
        llSetTextureAnim(ANIM_ON | SMOOTH | LOOP, face, 1, 1, 1.0, 1.0, rate);
        llRemoveInventory(llGetScriptName());
    }

}
