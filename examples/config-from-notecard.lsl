// config-from-notecard

// Reads a notecard with contents similar to this:
//
//   # config notecard - this is a comment
//   debug=no
//   # set channel to the magic number
//   channel=42

// Use channel 66 by default
integer CHANNEL = 66;

// Be quiet by default
integer VERBOSE = FALSE;

// Variables used by the notecard reading process
string CONFIG_NOTECARD = "notecard";
key card_query;
integer line;

default {
    state_entry() {
        if (llGetInventoryType(CONFIG_NOTECARD) == INVENTORY_NOTECARD) {
            // Start reading the notecard
            card_query = llGetNotecardLine(CONFIG_NOTECARD, line=0);
        }
    }

    dataserver(key queryid, string data) {
        if (queryid == card_query) {
            if ( data != EOF ) {
                // Skip comments - lines beginning with a hash ('#')
                integer ix = llSubStringIndex(data, "#");
                if (ix != -1) {
                    if (ix == 0) {
                        // First character is a hash ('#')
                        data = "";
                    } else {
                        // Remove everything from the hash to the end of the line
                        data = llGetSubString(data, 0, ix-1);
                    }
                }

                // If the data looks like a key...
                if ((data != "") && (data != "#") && (((key)data) != NULL_KEY) ) {
                    // Look for key=value args
                    list kv = llParseString2List(data, ["="], []);
                    string _key = llList2String(kv, 0);
                    string _val = llList2String(kv, 1);

                    if (_key == "debug") {
                        // This is a fancy way to check for multiple ways to say "yup"
                        VERBOSE = (llListFindList(["true", "on", "yes", "1"], [llToLower(_val)]) >= 0);
                    }
                    else if (_key == "channel") {
                        // Set the new channel
                        CHANNEL = (integer)_val;
                    }
                }
                // Read the next line from the notecard
                card_query = llGetNotecardLine(CONFIG_NOTECARD, ++line);
            } else {
                // Done
                llOwnerSay("Listening on channel " + (string)CHANNEL);
            }
        }
    }

    changed(integer change) {
        // Reset the script if inventory is changed (ie, the notecard was edited)
        if (change & (CHANGED_OWNER | CHANGED_INVENTORY))
            llResetScript();
    }
}
