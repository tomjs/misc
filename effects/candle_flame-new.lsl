// Particles: candle flame
// built using the TBF Particle HUD

default{
    state_entry(){
        llLinkParticleSystem(LINK_THIS,[
            PSYS_PART_FLAGS,
                PSYS_PART_INTERP_COLOR_MASK|
                PSYS_PART_INTERP_SCALE_MASK|
                PSYS_PART_BOUNCE_MASK|
                PSYS_PART_WIND_MASK|
                PSYS_PART_FOLLOW_VELOCITY_MASK|
                PSYS_PART_EMISSIVE_MASK,
            PSYS_PART_START_COLOR,<1,1,0>,
            PSYS_PART_START_ALPHA,.8,
            PSYS_PART_END_COLOR,<.4,0,0>,
            PSYS_PART_END_ALPHA,.0,
            PSYS_PART_START_SCALE,<.03,.1,0>,
            PSYS_PART_END_SCALE,<.05,.08,0>,
            PSYS_PART_MAX_AGE,.2,
            PSYS_SRC_ACCEL,<0,0,.4>,
            PSYS_SRC_PATTERN,
                PSYS_SRC_PATTERN_ANGLE_CONE,
            PSYS_SRC_BURST_RATE,.01,
            PSYS_SRC_BURST_PART_COUNT,4,
            PSYS_SRC_BURST_RADIUS,.02,
            PSYS_SRC_BURST_SPEED_MIN,.05,
            PSYS_SRC_BURST_SPEED_MAX,.3,
            PSYS_SRC_ANGLE_BEGIN,.05
        ]);
    }
}
