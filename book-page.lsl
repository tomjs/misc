// Book page handler
// v2 - auto-switch between UUIDs and inventory textures, pre-cache textures

// Page side, filled in from object name
// left or right?
string SIDE = "";

// Adjust this to the correct face or use ALL_SIDES
integer FACE = 4;

// Set a face to use for caching
integer CACHE_FACE = 0;

// Reset to system transparent
key DEFAULT_TEXTURE = TEXTURE_TRANSPARENT;

// Show debugging info
integer VERBOSE = FALSE;

debug(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

send(string message) {
    llMessageLinked(LINK_ALL_OTHERS, 1, message, "");
    debug("send: " + message);
}

default {
    state_entry() {
        SIDE = llGetObjectName();
    }

    link_message(integer send_num, integer num, string msg, key id) {
        debug("rcv: " + (string)num + ": " + msg + ": " + (string)id);
        if (num == -1 && msg == "reset") {
            // Reset
            llSetTexture(DEFAULT_TEXTURE, ALL_SIDES);
        }
        else if (num == -1 && msg == "debug") {
            VERBOSE = !VERBOSE;
        }
        else if (num == 1 && msg == SIDE) {
            // Display texture in id
            llSetTexture(id, FACE);
        }
        else if (num == 2 && msg == SIDE) {
            // Pre-cache texture in id
            llSetTexture(id, CACHE_FACE);
        }
    }

    touch_start(integer total_number) {
        send(SIDE);
    }
}
