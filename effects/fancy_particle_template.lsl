// particle script template
//
// v1   Fancy version with multiple effects
//
// Commands:
// on
// off
// toggle
// show
// hide
//
// Example:
// /40 on   - turns on the particles

// Listen channel
// Set the channel this script listens on here, this is the number
// following the '/' in local chat for the command.
integer CHANNEL = 40;

// Set to FALSE to listen to any avatar command
integer OWNER_ONLY = TRUE;
key listen_key = NULL_KEY;

integer particle_state = 0;
integer num_links = 0;

integer VERBOSE = FALSE;
log(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}

//
// Here is an example of what Firestorm's Particle Editor produces.  You
// need to copy the lines between ==begin== and ==end== as shown below into
// something like the following:

//default
//{
//    state_entry()
//    {
//        llParticleSystem(
//        [
// ==begin==
//            PSYS_SRC_PATTERN,PSYS_SRC_PATTERN_EXPLODE,
//            PSYS_SRC_BURST_RADIUS,0,
//            PSYS_SRC_ANGLE_BEGIN,0,
//            PSYS_SRC_ANGLE_END,0,
//            PSYS_SRC_TARGET_KEY,llGetKey(),
//            PSYS_PART_START_COLOR,<1.000000,1.000000,1.000000>,
//            PSYS_PART_END_COLOR,<1.000000,1.000000,1.000000>,
//            PSYS_PART_START_ALPHA,1,
//            PSYS_PART_END_ALPHA,1,
//            PSYS_PART_START_GLOW,0,
//            PSYS_PART_END_GLOW,0,
//            PSYS_PART_BLEND_FUNC_SOURCE,PSYS_PART_BF_SOURCE_ALPHA,
//            PSYS_PART_BLEND_FUNC_DEST,PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA,
//            PSYS_PART_START_SCALE,<0.500000,0.500000,0.000000>,
//            PSYS_PART_END_SCALE,<0.500000,0.500000,0.000000>,
//            PSYS_SRC_TEXTURE,"",
//            PSYS_SRC_MAX_AGE,0,
//            PSYS_PART_MAX_AGE,5,
//            PSYS_SRC_BURST_RATE,0.3,
//            PSYS_SRC_BURST_PART_COUNT,5,
//            PSYS_SRC_ACCEL,<0.000000,0.000000,0.000000>,
//            PSYS_SRC_OMEGA,<0.000000,0.000000,0.000000>,
//            PSYS_SRC_BURST_SPEED_MIN,0.5,
//            PSYS_SRC_BURST_SPEED_MAX,0.5,
//            PSYS_PART_FLAGS,
//                0
// ==end==
//        ]);
//    }
//}

// copy the lines so it looks like this:

list white_balls = [
// ==begin==
            PSYS_SRC_PATTERN,PSYS_SRC_PATTERN_EXPLODE,
            PSYS_SRC_BURST_RADIUS,0,
            PSYS_SRC_ANGLE_BEGIN,0,
            PSYS_SRC_ANGLE_END,0,
            PSYS_SRC_TARGET_KEY,llGetKey(),
            PSYS_PART_START_COLOR,<1.000000,1.000000,1.000000>,
            PSYS_PART_END_COLOR,<1.000000,1.000000,1.000000>,
            PSYS_PART_START_ALPHA,1,
            PSYS_PART_END_ALPHA,1,
            PSYS_PART_START_GLOW,0,
            PSYS_PART_END_GLOW,0,
            PSYS_PART_BLEND_FUNC_SOURCE,PSYS_PART_BF_SOURCE_ALPHA,
            PSYS_PART_BLEND_FUNC_DEST,PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA,
            PSYS_PART_START_SCALE,<0.500000,0.500000,0.000000>,
            PSYS_PART_END_SCALE,<0.500000,0.500000,0.000000>,
            PSYS_SRC_TEXTURE,"",
            PSYS_SRC_MAX_AGE,0,
            PSYS_PART_MAX_AGE,5,
            PSYS_SRC_BURST_RATE,0.3,
            PSYS_SRC_BURST_PART_COUNT,5,
            PSYS_SRC_ACCEL,<0.000000,0.000000,0.000000>,
            PSYS_SRC_OMEGA,<0.000000,0.000000,0.000000>,
            PSYS_SRC_BURST_SPEED_MIN,0.5,
            PSYS_SRC_BURST_SPEED_MAX,0.5,
            PSYS_PART_FLAGS,
                0
// ==end==
];

// "white_balls" is the name for the particle effect
// you use it below when selecting which effect to turn on
// each block of particle definitions needs to have a different name

list default_particles = [];

particles_link(integer link, list particles) {
        llLinkParticleSystem(link, particles);
}

particles_off() {
    log("particles off");
    particle_state = -1;
    integer i;
    for(i=0; i <= num_links; ++i) {
        log("link: "+(string)i);
        llLinkParticleSystem(i, []);
    }
}

particles_on() {
    log("particles on");
    particle_state = 1;
    integer i;
    for(i=0; i <= num_links; ++i) {
        particles(i);
    }
}

toggle() {
    if (particle_state > 0) {
        particles_off();
    } else {
        particles_on();
    }
}

hide() {
    llSetAlpha(0.0, ALL_SIDES);
}

show() {
    llSetAlpha(1.0, ALL_SIDES);
}

reset() {
    num_links = llGetObjectPrimCount(llGetKey());
//    if (num_links < 1) num_links = 1;
    log("reset(): " + (string)num_links);
    particles_off();
//    hide();

    if (OWNER_ONLY) {
        listen_key = llGetOwner();
    }
    llListen(CHANNEL, "", listen_key, "");
    log("listening on " + (string)CHANNEL);
}

default {
    state_entry() {
        reset();
    }

    listen(integer channel, string name, key id, string message) {
        // Parse command
        string cmd = "";
        string args = "";
        integer idx = llSubStringIndex(message, " ");
        if (idx == -1) {
            cmd = message;
            args = "";
        } else {
            cmd = llGetSubString(message, 0, idx-1);
            args = llDeleteSubString(message, 0, idx);
        }
        cmd = llToLower(cmd);
        log("cmd: " + cmd);

        // Parse a number too?
        integer num = (integer)cmd;

        if (cmd == "reset") {
            reset();
        } else if (cmd == "off") {
            particles_off();
        } else if (cmd == "on") {
            particles_on();
        } else if (cmd == "toggle") {
            toggle();
        } else if (cmd == "show") {
            show();
        } else if (cmd == "hide") {
            hide();
        } else if (cmd == "select") {
            if (num == 0) {
                default_particles = [];
            } else if (num == 2) {
                default_particles = white_balls;
            }
        } else if (cmd == "debug") {
            VERBOSE = !VERBOSE;
        }
    }

    changed(integer change) {
        if (change & CHANGED_LINK){
            reset();
        }
    }
}
