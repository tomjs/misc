// fire swamp
// v1   Fancy version with multiple effects
// v2   Attempt to ensure they are off
//
// Commands:
// on
// off
// toggle
// show
// hide
// touch    - turn on 'touch to toggle'
// notouch  - turn off 'touch to toggle'
//
// Example:
// /40 on   - turns on the particles

// Listen channel
// Set the channel this script listens on here, this is the number
// following the '/' in local chat for the command.
integer CHANNEL = 40;

// Set to FALSE to listen to any avatar command
integer OWNER_ONLY = FALSE;
key listen_key = NULL_KEY;

integer VERBOSE = FALSE;

// Set TRUE to enable touch to toggle particles on/off
integer TOUCH_OK = TRUE;

integer particle_state = 0;
integer num_links = 0;

integer FLAGS;
list SMOKE;
list FIRE1;
list FIRE2;

init_particles() {
    FLAGS = 0 |
        PSYS_PART_EMISSIVE_MASK |
        PSYS_PART_INTERP_COLOR_MASK |
        PSYS_PART_INTERP_SCALE_MASK;

    SMOKE = [
            PSYS_SRC_PATTERN,PSYS_SRC_PATTERN_ANGLE_CONE,
            PSYS_SRC_BURST_RADIUS,0,
            PSYS_SRC_ANGLE_BEGIN,0.3,
            PSYS_SRC_ANGLE_END,0.1,
            PSYS_PART_START_COLOR,<0.473785,0.461502,0.424652>,
            PSYS_PART_END_COLOR,<1.000000,1.000000,1.000000>,
            PSYS_PART_START_ALPHA,0.3,
            PSYS_PART_END_ALPHA,0.05,
            PSYS_PART_START_GLOW,0,
            PSYS_PART_END_GLOW,0,
            PSYS_PART_BLEND_FUNC_SOURCE,PSYS_PART_BF_SOURCE_ALPHA,
            PSYS_PART_BLEND_FUNC_DEST,PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA,
            PSYS_PART_START_SCALE,<1.000000,1.500000,0.000000>,
            PSYS_PART_END_SCALE,<3.000000,3.000000,0.000000>,
            PSYS_SRC_TEXTURE,"",
            PSYS_SRC_MAX_AGE,1.2,
            PSYS_PART_MAX_AGE,10,
            PSYS_SRC_BURST_RATE,0.3,
            PSYS_SRC_BURST_PART_COUNT,5,
            PSYS_SRC_ACCEL,<0.000000,0.000000,0.000000>,
            PSYS_SRC_OMEGA,<0.000000,0.000000,0.000000>,
            PSYS_SRC_BURST_SPEED_MIN,0.5,
            PSYS_SRC_BURST_SPEED_MAX,0.7,
            PSYS_PART_FLAGS, FLAGS
    ];

    FIRE1 = [
            PSYS_SRC_PATTERN,PSYS_SRC_PATTERN_ANGLE_CONE,
            PSYS_SRC_BURST_RADIUS,0.2,
            PSYS_SRC_ANGLE_BEGIN,0.6,
            PSYS_SRC_ANGLE_END,-0.6,
            PSYS_PART_START_COLOR,<0.573987,0.788086,0.236914>,
            PSYS_PART_END_COLOR,<0.675067,0.517516,0.253058>,
            PSYS_PART_START_ALPHA,0.5,
            PSYS_PART_END_ALPHA,0,
            PSYS_PART_START_GLOW,0.1,
            PSYS_PART_END_GLOW,0.1,
            PSYS_PART_BLEND_FUNC_SOURCE,PSYS_PART_BF_SOURCE_ALPHA,
            PSYS_PART_BLEND_FUNC_DEST,PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA,
            PSYS_PART_START_SCALE,<1.100000,0.500000,0.000000>,
            PSYS_PART_END_SCALE,<0.500000,0.500000,0.000000>,
            PSYS_SRC_TEXTURE,"82b9b60a-7146-c38d-63ea-42bd60ae1ab6",
            PSYS_SRC_MAX_AGE,1.4,
            PSYS_PART_MAX_AGE,1.4,
            PSYS_SRC_BURST_RATE,0.01,
            PSYS_SRC_BURST_PART_COUNT,200,
            PSYS_SRC_ACCEL,<0.300000,0.000000,2.200000>,
            PSYS_SRC_OMEGA,<-0.300000,0.100000,0.300000>,
            PSYS_SRC_BURST_SPEED_MIN,1.3,
            PSYS_SRC_BURST_SPEED_MAX,0.7,
            PSYS_PART_FLAGS, FLAGS
    ];

    FIRE2 = [
            PSYS_SRC_PATTERN,PSYS_SRC_PATTERN_ANGLE_CONE,
            PSYS_SRC_BURST_RADIUS,0.1,
            PSYS_SRC_ANGLE_BEGIN,0.3,
            PSYS_SRC_ANGLE_END,-0.3,
            PSYS_PART_START_COLOR,<0.373987,0.588086,0.736914>,
            PSYS_PART_END_COLOR,<0.375067,0.717516,0.953058>,
            PSYS_PART_START_ALPHA,1,
            PSYS_PART_END_ALPHA,0,
            PSYS_PART_START_GLOW,0.2,
            PSYS_PART_END_GLOW,0.1,
            PSYS_PART_BLEND_FUNC_SOURCE,PSYS_PART_BF_SOURCE_ALPHA,
            PSYS_PART_BLEND_FUNC_DEST,PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA,
            PSYS_PART_START_SCALE,<1.100000,0.500000,0.000000>,
            PSYS_PART_END_SCALE,<0.500000,0.500000,0.000000>,
            PSYS_SRC_TEXTURE,"82b9b60a-7146-c38d-63ea-42bd60ae1ab6",
            PSYS_SRC_MAX_AGE,0.7,
            PSYS_PART_MAX_AGE,0.7,
            PSYS_SRC_BURST_RATE,0.01,
            PSYS_SRC_BURST_PART_COUNT,200,
            PSYS_SRC_ACCEL,<0.300000,0.000000,4.200000>,
            PSYS_SRC_OMEGA,<-0.300000,0.100000,0.300000>,
            PSYS_SRC_BURST_SPEED_MIN,2.3,
            PSYS_SRC_BURST_SPEED_MAX,1.7,
            PSYS_PART_FLAGS, FLAGS
    ];
}

log(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}

list default_particles = [];

particles_link(integer link, list particles) {
    llLinkParticleSystem(link, particles);
}

particles_off() {
    log("particles off");
    particle_state = -1;
    llLinkParticleSystem(1, []);
    llLinkParticleSystem(2, []);
    llLinkParticleSystem(3, []);
}

particles_on() {
    log("particles on");
    llLinkParticleSystem(3, FIRE1);
    llSleep(0.2);
    llLinkParticleSystem(1, SMOKE);
    llSleep(0.2);
    llLinkParticleSystem(2, FIRE2);
}

toggle() {
    if (particle_state > 0) {
        particles_off();
    } else {
        particles_on();
    }
}

hide() {
    llSetLinkAlpha(LINK_SET, 0.0, ALL_SIDES);
}

show() {
    llSetLinkAlpha(LINK_SET, 1.0, ALL_SIDES);
}

reset() {
    num_links = llGetObjectPrimCount(llGetKey());
    log("reset(): " + (string)num_links);

    particles_off();
    init_particles();

    if (OWNER_ONLY) {
        listen_key = llGetOwner();
        log("owner only mode");
    } else {
        log("all allowed mode");
    }

    llListen(CHANNEL, "", listen_key, "");
    log("listening on " + (string)CHANNEL);
}

default {
    state_entry() {
        reset();
    }

    listen(integer channel, string name, key id, string message) {
        // Parse command
        string cmd = "";
        string args = "";
        integer idx = llSubStringIndex(message, " ");
        if (idx == -1) {
            cmd = message;
            args = "";
        } else {
            cmd = llGetSubString(message, 0, idx-1);
            args = llDeleteSubString(message, 0, idx);
        }
        cmd = llToLower(cmd);
        log("cmd: " + cmd);

        // Parse a number too?
        integer num = (integer)cmd;

        if (cmd == "reset") {
            reset();
        } else if (cmd == "off") {
            particles_off();
        } else if (cmd == "on") {
            particles_on();
        } else if (cmd == "fire") {
            particles_on();
            llSleep(2.0);
            particles_off();
        } else if (cmd == "toggle") {
            toggle();
        } else if (cmd == "show") {
            show();
        } else if (cmd == "hide") {
            hide();
        } else if (cmd == "touch") {
            TOUCH_OK = TRUE;
        } else if (cmd == "notouch") {
            TOUCH_OK = FALSE;
        } else if (cmd == "debug") {
            VERBOSE = !VERBOSE;
            log("Debug " + (string)VERBOSE);
        }
    }

    changed(integer change) {
        if (change & CHANGED_LINK){
            reset();
        }
    }

    touch_start(integer total_number) {
        if (TOUCH_OK) {
            particles_on();
//            toggle();
        }
    }
}
