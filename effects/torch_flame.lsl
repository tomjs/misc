// torch flame

startParticles() {
    llParticleSystem([PSYS_PART_FLAGS, 0| PSYS_PART_EMISSIVE_MASK| PSYS_PART_BOUNCE_MASK| PSYS_PART_INTERP_COLOR_MASK| PSYS_PART_INTERP_SCALE_MASK| PSYS_PART_FOLLOW_VELOCITY_MASK,
// PSYS_PART_WIND_MASK|
                        PSYS_SRC_BURST_RATE, 0.06,
                        PSYS_SRC_BURST_RADIUS, 0.03,
                        PSYS_SRC_BURST_PART_COUNT, 50,
                        PSYS_SRC_ANGLE_BEGIN, 0.3,
                        PSYS_SRC_ANGLE_END, 2.7,
                        PSYS_SRC_OMEGA, <0.0,0.0,1.9>,
                        PSYS_PART_MAX_AGE, 1.8,
                        PSYS_SRC_MAX_AGE, 0.0,
                        PSYS_SRC_BURST_SPEED_MIN, 0.03,
                        PSYS_SRC_BURST_SPEED_MAX, 0.07,
                        PSYS_SRC_TEXTURE,"",
                        PSYS_PART_START_ALPHA, 0.99,
                        PSYS_PART_END_ALPHA, 0.5,
                        PSYS_PART_START_COLOR, <0.8,  0.6,  0.9>,
                        PSYS_PART_END_COLOR,   <0.8,  0.5,  0.0>,
                        PSYS_PART_START_SCALE, <0.10, 0.10,  0.0>,
                        PSYS_PART_END_SCALE,   <0.07, 0.25, 0.0>,
                        PSYS_SRC_ACCEL, <0.0,0.0,0.2>,
                        PSYS_SRC_PATTERN, PSYS_SRC_PATTERN_ANGLE_CONE
 ]);

}

default {
    state_entry() {
       startParticles();
    }

    on_rez(integer start_param) {
        llResetScript();
    }
}
