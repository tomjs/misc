// no-prim-all - Reset all prim properties
// self-deleting

default {
    state_entry() {
        // Reset animation properties on all faces
        llSetTextureAnim(0, ALL_SIDES, 1, 1, 0.0, TWO_PI, 0.0);

        // Reset the floating text
        llSetText("", ZERO_VECTOR, 1.0);

        // Loop through all links in the object
        integer num_links = llGetNumberOfPrims();
        integer i;
        for(i=0; i <= num_links; ++i) {
            // Reset particles
            llLinkParticleSystem(i, []);

            // Reset sit targets
            llLinkSitTarget(i, ZERO_VECTOR, ZERO_ROTATION);
        }

        // Reset prim media
        for (; i < llGetLinkNumberOfSides(llGetLinkNumber()); i++) {
            integer clearMediaSucceeded = llClearPrimMedia(i);
        }

        // Reset target omega
        llTargetOmega(ZERO_VECTOR, 0, 0);

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
