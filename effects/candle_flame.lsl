// candle flame

startParticles() {
    llParticleSystem([PSYS_PART_FLAGS, 0| PSYS_PART_EMISSIVE_MASK| PSYS_PART_BOUNCE_MASK| PSYS_PART_INTERP_COLOR_MASK| PSYS_PART_INTERP_SCALE_MASK| PSYS_PART_WIND_MASK| PSYS_PART_FOLLOW_VELOCITY_MASK,
                        PSYS_SRC_BURST_RATE, 0.01,
                        PSYS_SRC_BURST_RADIUS, 0.0,
                        PSYS_SRC_BURST_PART_COUNT, 05,
                        PSYS_SRC_ANGLE_BEGIN, 0.05,
                        PSYS_SRC_ANGLE_END, 0.0,
                        PSYS_SRC_OMEGA, <0.0,0.0,0.0>,
                        PSYS_PART_MAX_AGE, 0.4,
                        PSYS_SRC_MAX_AGE, 0.0,
                        PSYS_SRC_BURST_SPEED_MIN, 0.05,
                        PSYS_SRC_BURST_SPEED_MAX, 0.07,
                        PSYS_SRC_TEXTURE,"",
                        PSYS_PART_START_ALPHA, 0.3,
                        PSYS_PART_END_ALPHA, 0.0,
                        PSYS_PART_START_COLOR, <1.0,1.0,0.0>,
                        PSYS_PART_END_COLOR, <0.4,0.0,0.0>,
                        PSYS_PART_START_SCALE, <0.03,0.03,0.0>,
                        PSYS_PART_END_SCALE, <0.05,0.05,0.0>,
                        PSYS_SRC_ACCEL, <0.0,0.0,0.5>,
                        PSYS_SRC_PATTERN, PSYS_SRC_PATTERN_ANGLE_CONE
 ]);

}

default {
    state_entry() {
       startParticles();
    }

    on_rez(integer start_param) {
        llResetScript();
    }
}
