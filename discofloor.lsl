//======================================================================
// Low Lag 6 x 6 Disco Floor -  Siggy Romulus - Get_Toe() Script
//----------------------------------------------------------------------
// Use a single script to control 36 randomly changing dancefloor
// squares
//
// lt1 - Major changes

//======================================================================
// Variables
//----------------------------------------------------------------------

// Set to the number of light prim in the floor
integer NUM_LIGHTS = 36;

// Set to the number of light prim to change each cycle
integer NUM_CHANGE = 20;

// Pause between color change cycles
float INTERVAL = 0.5;

// Our command channel
integer CHANNEL = 3302;
integer LISTEN_HANDLER;

// Or set to ALL_SIDES to catch'em all
integer COLOR_FACE = 0;

// Overall set of colors and a list to randomise

integer CURRENT;    // current place in the color list

// These are the same six colors, each repeated 3 time to let us change up
// to 18 squares at a time
list COLORSET = [
   <1.0, 0.0, 0.0>, <1.0, 1.0, 0.0>, <0.0, 1.0, 0.0>,
   <0.0, 1.0, 1.0>, <0.5, 0.5, 1.0>, <1.0, 0.0, 1.0>,
   <1.0, 0.0, 0.0>, <1.0, 1.0, 0.0>, <0.0, 1.0, 0.0>,
   <1.0, 0.0, 0.0>, <1.0, 1.0, 0.0>, <0.0, 1.0, 0.0>,
   <0.0, 1.0, 1.0>, <0.5, 0.5, 1.0>, <1.0, 0.0, 1.0>,
   <1.0, 0.0, 0.0>, <1.0, 1.0, 0.0>, <0.0, 1.0, 0.0>,
   <1.0, 0.0, 0.0>, <1.0, 1.0, 0.0>, <0.0, 1.0, 0.0>,
   <0.0, 1.0, 1.0>, <0.5, 0.5, 1.0>, <1.0, 0.0, 1.0>,
   <1.0, 0.0, 0.0>, <1.0, 1.0, 0.0>, <0.0, 1.0, 0.0>,
   <0.0, 1.0, 1.0>, <0.5, 0.5, 1.0>, <1.0, 0.0, 1.0>,
   <1.0, 0.0, 0.0>, <1.0, 1.0, 0.0>, <0.0, 1.0, 0.0>,
   <0.0, 1.0, 1.0>, <0.5, 0.5, 1.0>, <1.0, 0.0, 1.0>
];

list COLORUSE;
integer color_len;

integer VERBOSE = FALSE;

log(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}

Init() {
    integer x;

    for (x = 2; x < NUM_LIGHTS+2; x++) {
        llSetLinkPrimitiveParamsFast(x, [
            PRIM_COLOR, ALL_SIDES, <0,0,0>, 1.0,        // Set all sides black (off)
            PRIM_FULLBRIGHT, ALL_SIDES, TRUE,           // Make it all bright
            PRIM_GLOW, ALL_SIDES, 0.0                   // And twiddle the glow
        ]);
    }
    COLORUSE = llListRandomize(COLORSET, 1);        // Scramble the colors
    COLORUSE = COLORSET;
    for (x = 2; x < NUM_LIGHTS+2; x++) {
        llSetLinkColor(x, llList2Vector(COLORUSE, x-2), COLOR_FACE);
    }
    COLORUSE = llListRandomize(COLORSET, 1);        // Rescramble them
    color_len = llGetListLength(COLORUSE);
}

Update_Squares() {
    integer x;
    integer y;

    for (x = 0; x < NUM_CHANGE; x++) {
        y = (integer)llFrand((float)NUM_LIGHTS);
        y += 2;

        llSetLinkColor(y, llList2Vector(COLORUSE, CURRENT), COLOR_FACE);

        CURRENT++;

        if (CURRENT > color_len - 2)
            CURRENT = 0;

    }
}


default {
    state_entry() {
        llSetLinkColor(LINK_SET, <0,0,0>, ALL_SIDES);
        Init();
        LISTEN_HANDLER = llListen(CHANNEL, "", llGetOwner(), "");

        // Start by default
        llSetTimerEvent(INTERVAL);
    }

    listen(integer channel, string name, key id, string msg) {
        // Parse command
        string cmd = "";
        string args = "";
        integer idx = llSubStringIndex(msg, " ");
        if (idx == -1) {
            cmd = msg;
            args = "";
        } else {
            cmd = llGetSubString(msg, 0, idx-1);
            args = llDeleteSubString(msg, 0, idx);
        }
        cmd = llToLower(cmd);
        log("cmd: " + cmd);

        if (cmd == "start") {
            llSetLinkColor(1, <0,0,0.2>, ALL_SIDES);
            llSetTimerEvent(INTERVAL);
        }
        else if (cmd == "stop") {
            llSetLinkColor(1, <0,0,0>, ALL_SIDES);
            llSetTimerEvent(0.00);          // Reset timer
        }
        else if (msg == "reset") {
            llSetLinkColor(1, <0,0,0.2>, ALL_SIDES);
            Init();
            llSetTimerEvent(INTERVAL);
        }
    }

    timer() {
        Update_Squares();
    }


    on_rez(integer start_param) {
        Init();
    }
}
