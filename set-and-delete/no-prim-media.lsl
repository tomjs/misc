// no-prim-media - Reset texture animation
// self-deleting

default {
    state_entry() {
        // Reset prim media
        integer i;
        for (; i < llGetLinkNumberOfSides(llGetLinkNumber()); i++) {
            integer clearMediaSucceeded = llClearPrimMedia(i);
        }

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
