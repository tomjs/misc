// no-sit-target - Reset sit target
// self-deleting

default {
    state_entry() {
        // Reset sit targets in all prim in linkset
        integer i = llGetNumberOfPrims();
        for (; i >= 0; --i) {
            llLinkSitTarget(i, ZERO_VECTOR, ZERO_ROTATION);
        }

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
