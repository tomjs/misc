// config-from-desc - Get configuration from object Description field

// Use channel 66 by default
integer CHANNEL = 66;

// Enable sound by default
integer SOUND = TRUE;

// parse_args() sets global config directly from the contents of the object's Description field
// Multiple items are separated by a colon (':').
// Configs can either be a key=value pair or just a single key word to enable a feature
//
// Set the channel to 99:
//   channel=99
//
// Set the channel to -2231 and turn off sound:
//   channel=-2231:nosound

parse_args(string desc) {
    list _args = llParseStringKeepNulls(desc, [ ":" ], []);

    integer i = 0;
    string s = "";
    for (; i < llGetListLength(_args); ++i) {
        s = llList2String(_args, i);
        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);

        if (_key == "channel") {
            // Set channel
            CHANNEL = (integer)_val;
        }
        else if (_key == "nosound") {
            // Turn off sound
            SOUND = FALSE;
        }
    }
}

default {
    state_entry() {
        parse_args(llGetObjectDesc());
    }

    touch_start(integer total_number) {
        llOwnerSay("Channel: " + (string)CHANNEL);
        llOwnerSay("Sound: " + (string)SOUND);
    }
}
