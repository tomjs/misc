// no-float - Set float text from object name or string
// self-deleting

default {
    state_entry() {
        // Reset the floating text
        llSetText("", ZERO_VECTOR, 1.0);

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
