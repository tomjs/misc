// no-animation - Reset texture animation on all faces
// self-deleting

default {
    state_entry() {
        // Reset animation properties on all faces
        llSetTextureAnim(0, ALL_SIDES, 1, 1, 0.0, TWO_PI, 0.0);

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
