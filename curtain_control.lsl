// curtain_control

// v1 - Original mods for lt curtains
// v2 - Fix off-by-one step count
// v3 - Support path cutting cylinders
// v4 - Rewrite to add fade open/closed, configure from notecard
// v5 - Fix auth bug from HUD
// v6 - Add sound, optimize resize()
// v7 - Fix already open/closed check
// v8 - Fix auth users reset bug, sound delay
// v9 - Reset after reading notecard
// v10 - Add memory constraint
// v11 - Internal cleanups and restructuring
// v12 - Fix cylinder mode

// Uses an optional Configuration Notecard (".config") for settings.  The
// possible settings are listed below near the global values they set, here
// is a short example (Nore that comments are lines beginning with '#')::

// allow=tomjs
// #channel=4242
// fade=true
// #touch=false
// #cylinder=true
// duration=1.0
// #steps=50
// #start_size=<>
// #stop_size=<>
// #start_alpha=1.0
// #stop_alpha=0.0
// #start_alpha_back=0.8

// ==========

// Define the large and small sizes of the prim here:
// (if the prim is sliced or path cut, it will appear to be half size on the affected dimension)
// This is path cut for cylinders and slice for boxes.  Normally the box is similar to
// many doors in SL, slice cut so only half of the box is visible to allow one end
// to appear stationary as the box is resized.

// Config: start_size=<>
vector  START_SCALE = <0.10000, 1.00000, 1.00000>;
// Config: stop_size=<>
vector  STOP_SCALE = <0.10000, 1.00000, 0.30000>;
float   MID_SCALE = 0.0;

// These are used to keep the edge of the curtain texture aligned with the edge of
// the prim as it is resized.  This avoids the 'squishing' of the texture to make it
// look like it is being rolled up or simply slid away.
vector TEX_START_SCALE = <1.00000, 2.00000, 0.00000>;
vector TEX_STOP_SCALE = <1.00000, 0.20000, 0.00000>;
vector TEX_START_OFFSET = <0.0, 0.5, 0.0>;
vector TEX_STOP_OFFSET = <0.0, -0.4, 0.0>;

// The alpha values for open/closed.  The back face has a separate closed value so it
// can be left partially transparent to see the audience from the stage when closed.
// Config: start_alpha=X.X
float START_ALPHA = 1.0;
// Config: stop_alpha=X.X
float STOP_ALPHA = 0.0;
// Config: start_alpha_back=X.X
float START_ALPHA_BACK = 0.8;

// Config: steps=n
integer STEPS = 30;

// Config: duration=N.N
float DURATION = 4.0;

// Track the open/closed state of the curtain
// 1 == closed, -1 == open
integer toggle_state = 1;

// Local command channel
// Config: channel=NNN
integer CHANNEL = 4242;
integer MENU_CHANNEL = 0;

// Curtain face - depending on rotation, is usually one of the red or green axis
// faces on a box prim.
// Config: face=N
integer FACE = 2;
// Config: back_face=N
integer BACK_FACE = 4;

// Set FALSE to change curtain size, TRUE to fade
// Config: fade=true
integer FADE_OPEN = FALSE;

// Set to TRUE to allow touching the curtain itself to display the open/close dialog
// Config: touch=true
integer TOUCH_OPEN = FALSE;

// Set TRUE if path cutting a cylinder rather than resizing
// Config: cylinder=true
integer CYLINDER = FALSE;

// Texture rotation - 0 for cylinders and vertical movement, usually
// 90 for horizontal movement (in degrees)
integer TEXTURE_ROT = 0;

// Spew debug info
integer VERBOSE = FALSE;

integer MEM_LIMIT = 32000;

// Config: allow=XXXX
// Only set DEFAULT_AUTHORIZED_USERS here
list DEFAULT_AUTHORIZED_USERS = [
];
list AUTHORIZED_USERS;

// The name of the configuration notecard
string CONFIG_NOTECARD = ".config";

key config_handle;
integer config_count;
integer prim_count;

// Open and close sounds
key open_sound = "";  // roll-up
key close_sound = "";  // roll-down
//key open_sound = "919817c9-aaf7-abac-0e8c-ee6388b342fb";  // roll-up
//key close_sound = "174bd34f-004d-9cc3-1750-eb5fb3b55c3f";  // roll-down

// 0.0 is off, 1.0 is loudest
float volume = 1.0;

// Dialog Box

integer LINK_CMD = 998765;

// Listen handles
integer cmd_handle = 0;
integer dialog_handle;

// Save original sizes
list orig_params;
list save_params;

// Buttons for the main operation dialog
list MENU = ["Open", "Close"];
// Buttons for the config dialog
list CONFIG_MENU = ["Fade", "NoTouch", "Exit", "Raise", "Touch", "Back"];

log(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

// Display the operation dialog
show_dialog(key id) {
    llListenRemove(dialog_handle);
    list menu = MENU;
    if (id == llGetOwnerKey(llGetKey())) {
        // Add the Config button for the owner
        menu += "Config";
    }
    string msg = "Curtain Control";
    dialog_handle = llListen(MENU_CHANNEL, "", NULL_KEY, "");
    llDialog(id, msg, menu, MENU_CHANNEL);
}

// Display the configuration dialog
config_dialog(key id) {
    llListenRemove(dialog_handle);
    string mode = "Raise";
    if (FADE_OPEN) mode = "Fade";
    string touchme = "False";
    if (TOUCH_OPEN) touchme = "True";
    string msg =
        "Curtain Control\n\n" +
        "Mode: " + mode + "\n" +
        "Touch: " + touchme + "\n";
    dialog_handle = llListen(MENU_CHANNEL, "", NULL_KEY, "");
    llDialog(id, msg, CONFIG_MENU, MENU_CHANNEL);
}

// Gets PRIM_SIZE (scale) or PRIM_TYPE (path cut) if CYLINDER is TRUE
vector get_params() {
    vector v;
    if (CYLINDER) {
        save_params = llGetLinkPrimitiveParams(LINK_THIS, [PRIM_TYPE]);
        v = llList2Vector(save_params, 2);
    } else {
        v = llGetScale();
    }
    return v;
}

// Sets given size directly
set_size(vector size) {
    if (CYLINDER) {
        save_params = llListReplaceList(save_params, [size], 2, 2);
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_TYPE] + save_params);
    } else {
        llSetScale(size);
    }
}

reset() {
    // Tear down old stuffs
    llListenRemove(cmd_handle);

    // Reset auth user list
    AUTHORIZED_USERS = DEFAULT_AUTHORIZED_USERS;

    if (MENU_CHANNEL == 0) {
        MENU_CHANNEL = -CHANNEL;
        log("Menu: " + (string)MENU_CHANNEL);
    }

    // Start anew
    cmd_handle = llListen(CHANNEL, "", NULL_KEY, "");
    prim_count = llGetNumberOfPrims();

    // Get current prim shape
    orig_params = llGetLinkPrimitiveParams(LINK_THIS, [PRIM_TYPE]);
    save_params = orig_params;
    set_size(START_SCALE);

    // Save half the distance to the goal
    MID_SCALE = llVecDist(START_SCALE, STOP_SCALE) / 2;

    // Initialize to closed
    toggle_state = 1;
    TEX_START_SCALE = llGetTextureScale(FACE);
    TEX_START_OFFSET = llGetTextureOffset(FACE);
//    llScaleTexture(TEX_START_SCALE.x, TEX_START_SCALE.y, FACE);
//    llOffsetTexture(TEX_START_OFFSET.x, TEX_START_OFFSET.y, FACE);
//    llRotateTexture(TEXTURE_ROT * DEG_TO_RAD, FACE);
    llSetLinkAlpha(LINK_THIS, START_ALPHA, FACE);
    llSetLinkAlpha(LINK_THIS, START_ALPHA_BACK, BACK_FACE);

    llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
}

resize() {
    list tParams = llGetLinkPrimitiveParams(LINK_THIS, [PRIM_TEXTURE, FACE]);
    string tex = llList2String(tParams, 0);
    vector tscale = llList2Vector(tParams, 1);
    vector toffset = llList2Vector(tParams, 2);
    float trot = llList2Float(tParams, 3);

    float interval = DURATION/(float) STEPS;

    vector TexStep = (TEX_START_SCALE - TEX_STOP_SCALE) / STEPS;
    vector OffStep = -(TEX_STOP_OFFSET - TEX_START_OFFSET) / STEPS;

    vector wscale = get_params();

    vector ScaleStep = (START_SCALE - STOP_SCALE) / STEPS;  // Compute the scale augment per step
    integer i;
    for ( ; i <= STEPS; ++i ) {
        vector ww = wscale + ScaleStep * (float)i * toggle_state;
        vector tt = tscale + TexStep * (float)i * toggle_state;
        vector oo = toffset + OffStep * (float)i * toggle_state;

        if (CYLINDER) {
            save_params = llListReplaceList(save_params, [ww], 2, 2);
            llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_TYPE] + save_params);
        } else {
            llSetScale(ww);
        }

        llSetLinkPrimitiveParamsFast(
            LINK_THIS,
            [
                PRIM_TEXTURE,
                FACE,
                tex,
                tt,
                oo,
                trot // * DEG_TO_RAD
            ]
        );

        // It is more lag-friendly to incorporate a sleep per step
        // Rather than greatly increasing the number of steps
        llSleep(interval+0.005);
    }
    // set final position
    llSleep(interval*2);
    if (toggle_state > 0) {
        set_size(START_SCALE);
    } else {
        set_size(STOP_SCALE);
    }
}

fade() {
    float alpha;
    float back_alpha;
    if (toggle_state > 0) {
        alpha = STOP_ALPHA;
        back_alpha = STOP_ALPHA;
    } else {
        alpha = START_ALPHA;
        back_alpha = START_ALPHA_BACK;
    }
    float interval = DURATION/(float) STEPS;
    float step = (START_ALPHA - STOP_ALPHA) / (float) STEPS * -toggle_state;
    float back_step = (START_ALPHA_BACK - STOP_ALPHA) / (float) STEPS * -toggle_state;

    integer i;
    for ( ; i <= STEPS; ++i ) {
        // It is more lag-friendly to incorporate a sleep per step
        // Rather than greatly increasing the number of steps
        llSleep(interval);
        alpha -= step;
        back_alpha -= back_step;
        llSetLinkAlpha(LINK_THIS, alpha, FACE);
        llSetLinkAlpha(LINK_THIS, back_alpha, BACK_FACE);
    }

    // set final alpha
    llSleep(interval*2);
    if (toggle_state > 0) {
        llSetLinkAlpha(LINK_THIS, START_ALPHA, FACE);
        llSetLinkAlpha(LINK_THIS, START_ALPHA_BACK, BACK_FACE);
    } else {
        llSetLinkAlpha(LINK_THIS, STOP_ALPHA, FACE);
        llSetLinkAlpha(LINK_THIS, STOP_ALPHA, BACK_FACE);
    }
}

close() {
    if (toggle_state > 0) {
        // already closed
        return;
    }
    toggle_state = 1;
    log("CLOSE");
    if (close_sound != "") {
        llTriggerSound(close_sound, volume);
    }
    if (FADE_OPEN) {
        set_size(START_SCALE);
        fade();
    } else {
        vector _wscale = get_params();
        if (llVecDist(_wscale, STOP_SCALE) > MID_SCALE)  {
            // We're already grown...
            set_size(START_SCALE);
            return;
        }
        resize();
    }
}

open() {
    if (toggle_state < 0) {
        // already open
        return;
    }
    toggle_state = -1;
    if (open_sound != "") {
        llTriggerSound(open_sound, volume);
    }
    if (FADE_OPEN) {
        fade();
        set_size(STOP_SCALE);
    } else {
        vector _wscale = get_params();
        if (llVecDist(_wscale, START_SCALE) > MID_SCALE)  {
            // We're already closed...
            set_size(STOP_SCALE);
            return;
        }
        resize();
    }
}

toggle() {
    toggle_state *= -1;       // Switch between open and closed
    if (FADE_OPEN) {
        fade();
    } else {
        resize();
    }
}

command(string msg, key id) {
    log("command("+msg+", "+(string)id+")");
    string cmd = "";
    integer idx = llSubStringIndex(msg, " ");
    if (idx == -1) {
        cmd = msg;
    } else {
        cmd = llGetSubString(msg, 0, idx-1);
        msg = llDeleteSubString(msg, 0, idx);
    }
    cmd = llToLower(cmd);
    log("  cmd="+cmd);

    if (cmd == "reset") {
        reset();
    }
    else if (cmd == "debug") {
        VERBOSE = !VERBOSE;
        log("Debug toggled");
    }
    else if (cmd == "tog" || cmd == "toggle") {
        toggle();
    }
    else if (cmd == "close") {
        close();
    }
    else if (cmd == "open" || cmd == "grow") {
        open();
    }
    else if (cmd == "fade") {
        set_size(START_SCALE);
        FADE_OPEN = TRUE;
        config_dialog(id);
    }
    else if (cmd == "raise") {
        set_size(START_SCALE);
        FADE_OPEN = FALSE;
        config_dialog(id);
    }
    else if (cmd == "touch") {
        TOUCH_OPEN = TRUE;
        config_dialog(id);
    }
    else if (cmd == "notouch") {
        TOUCH_OPEN = FALSE;
        config_dialog(id);
    }
    else if (cmd == "config") {
        config_dialog(id);
    }
    else if (cmd == "back") {
        show_dialog(id);
    }
}

// authorized(ID)
// Return TRUE if:
// * owner
// * group matches object group
// * key in AUTHORIZED_USERS
// * name(key) in AUTHORIZED_USERS
integer authorized(key id) {
    string username =  llGetUsername(id);
    log("key->username: " + username);
    if (username == "") {
        // id is a prim
        id = llGetOwnerKey(id);
        log("new id: " + (string)id);
    }
    return (
        id == llGetOwnerKey(llGetKey()) ||
        llSameGroup(id) ||
        llListFindList(AUTHORIZED_USERS, [(string)id]) >= 0 ||
        llListFindList(AUTHORIZED_USERS, [username]) >= 0
    );
}

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
        if (llGetInventoryType(CONFIG_NOTECARD) == INVENTORY_NOTECARD) {
            log("Reading notecard " + CONFIG_NOTECARD);
            config_handle = llGetNotecardLine(CONFIG_NOTECARD, config_count = 0);
        } else {
            reset();
        }
    }

    on_rez(integer x) {
        llResetScript();
    }

    touch_start(integer total_number) {
        if (TOUCH_OPEN && authorized(llDetectedKey(0))) {
            show_dialog(llDetectedKey(0));
        }
    }

    listen(integer channel, string name, key id, string msg) {
        log("listen("+(string)channel+", "+name+", "+(string)id+", "+msg+")");
        if (channel == MENU_CHANNEL) {
            llListenRemove(dialog_handle);
        }
        if (authorized(id)) {
            // Process our commands
            command(msg, id);
        }
    }

    link_message(integer sender, integer num, string msg, key id) {
        if (num == LINK_CMD) {
            // Process our commands
            command(msg, id);
        }
    }

    dataserver(key queryid, string data) {
        if (queryid == config_handle) {
            // If this isn't the end of the file...
            if (data != EOF) {
                // Skip comments
                integer ix = llSubStringIndex(data,"#");
                if (ix != -1) {
                    if (ix == 0) {
                        data = "";
                    } else {
                        data = llGetSubString(data, 0, ix - 1);
                    }
                }

                // If the data looks like a key...
                if ((data != "") && (data != "#") && (((key)data) != NULL_KEY) ) {
                    // Look for key=value args
                    list kv = llParseString2List(data, ["="], []);
                    string _key = llList2String(kv, 0);
                    string _val = llList2String(kv, 1);

                    if (_key == "debug") {
                        VERBOSE = (llListFindList(["true", "on", "yes", "1"], [ llToLower(_val) ]) >= 0);
                    }
                    else if (_key == "channel") {
                        CHANNEL = (integer)_val;
                    }
                    else if (_key == "duration") {
                        DURATION = (float)_val;
                    }
                    else if (_key == "steps") {
                        STEPS = (integer)_val;
                    }
                    else if (_key == "face") {
                        FACE = (integer)_val;
                    }
                    else if (_key == "back_face") {
                        BACK_FACE = (integer)_val;
                    }
                    else if (_key == "start_size") {
                        START_SCALE = (vector)_val;
                    }
                    else if (_key == "stop_size") {
                        STOP_SCALE = (vector)_val;
                    }
                    else if (_key == "start_alpha") {
                        START_ALPHA = (float)_val;
                    }
                    else if (_key == "stop_alpha") {
                        STOP_ALPHA = (float)_val;
                    }
                    else if (_key == "start_alpha_back") {
                        START_ALPHA_BACK = (float)_val;
                    }
                    else if (_key == "allow") {
                        integer len = llGetListLength(AUTHORIZED_USERS);
                        AUTHORIZED_USERS = llListReplaceList(
                            AUTHORIZED_USERS,
                            [ _val ],
                            len,
                            len
                        );
                        log("added " + _val);
                    }
                    else if (_key == "fade") {
                        FADE_OPEN = (llListFindList(["true", "on", "yes", "1"], [ llToLower(_val) ]) >= 0);
                        log("fade="+(string)FADE_OPEN);
                    }
                    else if (_key == "touch") {
                        TOUCH_OPEN = (llListFindList(["true", "on", "yes", "1"], [ llToLower(_val) ]) >= 0);
                        log("touch="+(string)TOUCH_OPEN);
                    }
                    else if (_key == "cylinder") {
                        CYLINDER = (llListFindList(["true", "on", "yes", "1"], [ llToLower(_val) ]) >= 0);
                        log("cylinder="+(string)CYLINDER);
                    }
                }

                config_handle = llGetNotecardLine(CONFIG_NOTECARD, ++config_count);
            } else {
                // Done
                log("Listening on channel " + (string)CHANNEL);
                reset();
            }
        }
    }

    changed(integer change) {
        if (change & CHANGED_INVENTORY) {
            llResetScript();
        }
        if (change & CHANGED_SCALE) {
            vector v = get_params();
           llOwnerSay("Curtain has changed size:");
            if (toggle_state >= 0) {
               llOwnerSay("start_size=" + (string)v);
                START_SCALE = v;
            } else {
               llOwnerSay("stop_size" + (string)v);
                STOP_SCALE = v;
            }
        }
    }
}
