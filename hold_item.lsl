// hold pose
// v1

// Put in an object to be worn with an animation/pose and the anim
// will run when the object is attached.
//
// Optionally set an offset and rotation to be set the first time
// the object is attached.  If moved the movement will be remembered
// until the script is reset.

// When TRUE will reset position/rotation of object
// Reset by do_rot()
integer set_default = TRUE;

// Set the default position on attach
vector offset = <-0.03602, 0.03728, -0.05680>;
vector rot_deg = <23.35001, 68.64999, 194.05000>;
rotation rot;

string anim = "";
key owner;

integer mem_limit = 10000;
integer DEBUG = FALSE;

log(string msg) {
    if (DEBUG) {
        llOwnerSay(msg);
    }
}

do_rot() {
    if (set_default) {
        // Only happens the first time after a reset
        list li = llGetLinkPrimitiveParams(LINK_SET, [PRIM_ROT_LOCAL, PRIM_POS_LOCAL]);
        rotation local = llList2Rot(li,0);
        vector pos = llList2Vector(li,1);
        llSetLinkPrimitiveParamsFast(
            LINK_SET,
            [
                PRIM_POS_LOCAL, pos+offset,
                PRIM_ROT_LOCAL, rot*local
            ]
        );
        // causes flip every other attach?
        //rot.s *= -1;
        //offset *= -1;
    }
    set_default = FALSE;
}

default {
    state_entry() {
        llSetMemoryLimit(mem_limit);

        if (anim == "") {
            // Use the first animation in object inventory
            anim = llGetInventoryName(INVENTORY_ANIMATION, 0);
        }

        // Set up the default rotation
        rot = llEuler2Rot(rot_deg*DEG_TO_RAD);

        owner = llGetOwner();
        llRequestPermissions(owner, PERMISSION_TRIGGER_ANIMATION);
    }

    attach(key id) {
        if (id) {
            log("id="+(string)id);
            llRequestPermissions(id, PERMISSION_TRIGGER_ANIMATION);
            if (llGetAttached()) {
                log("att=TRUE");
                do_rot();
            }
            if (llGetPermissions() & PERMISSION_TRIGGER_ANIMATION) {
                log("start anim");
                llStartAnimation(anim);
            }
        } else {
            log("stop anim");
            llStopAnimation(anim);
        }
        log("Free mem: " + (string)llGetFreeMemory());
    }

    changed(integer change) {
        if (change & CHANGED_OWNER) {
            log("reset");
            llResetScript();
        }
    }
}
