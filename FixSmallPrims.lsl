///////////////////////////////////////////////////////////////////////////////
// FixSmallPrims2
// by Emma Nowhere
//
// Last modified: 02Jan2016
//
// How to use with menu:
// 1. Install this script in the root prim of a linked set of prims (aka "linkset")
// 2. Type /1fsp to show the menu
// 3. Hit the "Fix" button to fix all the small prims
// 4. Resize the linkset object to the desired size with the Editing Tools
// 5. Type /1fsp to show the menu again
// 6. Hit the "Finish" button to remove the script and finalize prim sizes
//
// (Advanced) How to use via chat commands:
// 1. Install this script in the root prim of a linked set of prims (aka "linkset")
// 2. Type /1fsp to show the menu
// 3. Type /1fsprun to fix all the small prims
// 4. Resize the linkset object to the desired size with the Editing Tools
// 5. Type /1fsprestore to return them to their original sizes
// 6. Type /1fspcleanup to remove the script and finalize prim sizes
//
// 02Jan2016 (tomjs)
// * modify to listen to any avatar (useful for fixing other people's objects)

// Listen to this key (select one)
key LISTEN_KEY = NULL_KEY;    // Listen to any avatar
//key LISTEN_KEY = llGetOwner();    // Listen only to owner

integer CHANNEL = 1;
integer MENU_CHANNEL = -1001;

list backupScales = [];
integer backupStored = FALSE;
 
list rescaleFlags = [];
 
backup() {
    if (!backupStored) {
        llSay(0, "Backing up prim sizes");
        backupScales = [];
        integer p = llGetNumberOfPrims();
        integer i = 0;
        for (i = 1; i <= p; i++)
        {
            list params = llGetLinkPrimitiveParams(i, [PRIM_SIZE]);
            vector scale = llList2Vector(params, 0);
            backupScales = backupScales + [scale];
        }
        backupStored = TRUE;
        llSay(0, "Prim sizes backed up");
    }
}
 
restore() {
    if (backupStored) {
        llSay(0, "Restoring previously backed up prim sizes");
        integer p = llGetNumberOfPrims();
        integer i = 0;
        for (i = 1; i <= p; i++)
        {
            vector scale = llList2Vector(backupScales, i - 1);
            llSetLinkPrimitiveParamsFast(i, [PRIM_SIZE, scale]);
        }
        llSay(0, "Previously backed up prim sizes restored");
    }
    rescaleFlags = [];
}
 
cleanup() {
    llSay(0, "Cleaning up FixSmallPrims data and finalizing prim sizes");
    integer p = llGetNumberOfPrims();
    integer i = 0;
    for (i = 1; i <= p; i++)
    {
        vector backupScale = llList2Vector(backupScales, i - 1);
        integer rescaleI = (i - 1) * 3;
        integer rescaleX = llList2Integer(rescaleFlags, rescaleI);
        integer rescaleY = llList2Integer(rescaleFlags, rescaleI + 1);
        integer rescaleZ = llList2Integer(rescaleFlags, rescaleI + 2);
 
        list params = llGetLinkPrimitiveParams(i, [PRIM_SIZE]);
        vector scale = llList2Vector(params, 0);
 
        if (rescaleX) {
            scale.x = backupScale.x;
        }
 
        if (rescaleY) {
            scale.y = backupScale.y;
        }
 
        if (rescaleZ) {
            scale.z = backupScale.z;
        }
 
        if (rescaleX || rescaleY || rescaleZ) {
            llSay(0, "Cleaning size of linked prim #" + (string)i);
            llSetLinkPrimitiveParamsFast(i, [PRIM_SIZE, scale]);
        }
    }
 
    llSay(0, "Deleting FixSmallPrims script");
    llRemoveInventory(llGetScriptName()); 
}
 
process() {
    restore();
    backup();
 
    llSay(0, "Starting fixing prims");
    rescaleFlags = [];
    integer p = llGetNumberOfPrims();
    integer i = 0;
    for (i = 1; i <= p; i++)
    {
        list params = llGetLinkPrimitiveParams(i, [PRIM_SIZE]);
        vector scale = llList2Vector(params, 0);
 
        integer rescaleX = FALSE;
        integer rescaleY = FALSE;
        integer rescaleZ = FALSE;
 
        if (scale.x < .015) {
            scale.x = .015;
            rescaleX = TRUE;
        }
 
        if (scale.y < .015) {
            scale.y = .015;
            rescaleY = TRUE;
        }
 
        if (scale.z < .015) {
            scale.z = .015;
            rescaleZ = TRUE;
        }
 
        if (rescaleX || rescaleY || rescaleZ) {
            llSay(0, "Fixing size of linked prim #" + (string)i);
            llSetLinkPrimitiveParamsFast(i, [PRIM_SIZE, scale]);
        }
        rescaleFlags = rescaleFlags + [rescaleX, rescaleY, rescaleZ];
    }
    llSay(0, "Done fixing prims\n\nResize your object and type /" + (string)CHANNEL + "fsp for menu when done.\n\n");
}
 
menu() {
    llDialog(llDetectedKey(0),
    "Fix Small Prims\n\nMake a backup of your object first.\n\nPlease choose an option:\n",
    ["Fix", "Finish"], MENU_CHANNEL);
}
 
default
{
    state_entry() 
    {
        llListen(CHANNEL, "", LISTEN_KEY, "");
        llListen(MENU_CHANNEL, "", LISTEN_KEY, "");
        llSay(0, "FixSmallPrims Ready");
        llSay(0, "Type /" + (string)CHANNEL + "fsp for menu");
    }
 
    on_rez(integer start_param) {
        llSay(0, "FixSmallPrims Installed");
        llSay(0, "Type /" + (string)CHANNEL + "fsp for menu");
    }    
 
    listen(integer channel, string name, key id, string message) 
    {
        if (message == "fsp") {
            menu();
        }
        else if (message == "fsptest") {
            llSay(0, "FixSmallPrims script is installed and ready");
            if (backupStored) {
                llSay(0, "Original prim sizes have been backed up");
            }
        }
        else if (message == "fspbackup") {
            backup();
        }
        else if (message == "fsprestore") {
            restore();
        }
        else if ((message == "fspcleanup") || (message == "Finish")) {        
            cleanup();                    
        }
        else if ((message == "fsprun") || (message == "Fix")) {
            process();            
        }
 
    }
}
