// book - Cycle through book pages
// v2 - auto-switch between UUIDs and inventory textures, pre-cache textures
// v3 - remove need for slave scripts
//
// Click to cycle through textures in inventory, click on the left side
// to go back and on the right side to go forward.

// Put the 'book page' script into each page prim to collect page clicks and
// handle texture displays


list Pages = [
    TEXTURE_TRANSPARENT,
    // add texture UUIDs here
    TEXTURE_TRANSPARENT
];

// Set to image advance time, 0 for no auto-advance
float time = 0;

// Local command channel
integer CHANNEL = 123;

// Display float text
integer SHOW_FLOAT = FALSE;

// Spew debug info
integer VERBOSE = FALSE;

// Object attributes - describes the linked prim to display the textures
// Link numbers are auto-detected based on link object names 'left' and 'right'
integer LeftLink = 0;
integer RightLink = 0;

// Link faces are hard-coded
integer LeftFace = 4;
integer RightFace = 4;

// Global state
integer Total;
integer Counter = -2;

// Listen handles
integer hChannel = 0;

debug(string txt) {
    if (VERBOSE) {
        llOwnerSay(txt);
    }
}

show() {
    debug("show: " + (string)Counter);
    key left = llList2String(Pages, Counter);
    llSetLinkPrimitiveParams(LeftLink, [PRIM_TEXTURE, LeftFace,  left, <1,1,1>, <0,0,0>, 0]);
    key right = llList2String(Pages, Counter+1);
    llSetLinkPrimitiveParams(RightLink, [PRIM_TEXTURE, RightFace,  right, <1,1,1>, <0,0,0>, 0]);
    debug("show: " + (string)left + " <-> " + (string)right);

    // Pre-cache the next set of textures
    integer counter_next = Counter + 2;
    if (counter_next >= Total) {
        counter_next = 0;
    }
    left =  llList2String(Pages, counter_next);
    llSetLinkPrimitiveParams(LeftLink, [PRIM_TEXTURE, LeftFace-1,  left, <1,1,1>, <0,0,0>, 0]);
    right =  llList2String(Pages, counter_next+1);
    llSetLinkPrimitiveParams(RightLink, [PRIM_TEXTURE, RightFace-1,  right, <1,1,1>, <0,0,0>, 0]);
    debug("cache: " + (string)left + " <-> " + (string)right);
}

next() {
    Counter++; Counter++;
    if(Counter >= Total) {
        Counter = 0;
    }
    show();
}

prev() {
    if (Counter <= 0) {
        Counter = Total - 2;
    } else {
        Counter--; Counter--;
    }
    show();
}

// Load the Pages list with inventory textures if present
load_pages() {
    Total = llGetInventoryNumber(INVENTORY_TEXTURE);
    if (Total == 0) {
        // If there are no textures in inventory, use the UUID list as-is
        Total = llGetListLength(Pages);
    } else {
        // Load the inventory textures and bookend with transparent textures
        Pages = [TEXTURE_TRANSPARENT];
        integer i;
        string name;
        list params;
        string s = "";
        for (i=0; i<Total; i++) {
            name = llGetInventoryName(INVENTORY_TEXTURE, i);
            Pages += [name];
            if (VERBOSE) {
                // Make a list of UUIDs suitable for copy-n-paste into the Pages initializer above
                llSetLinkPrimitiveParams(LeftLink ,[PRIM_TEXTURE, LeftFace-1, name, <1,1,1>, <0,0,0>, 0]);
                params = llGetLinkPrimitiveParams(LeftLink ,[PRIM_TEXTURE, LeftFace-1]);
                s += "\n    \"" + llList2String(params, 0) + "\",";
            }
        }
        Pages += [TEXTURE_TRANSPARENT];
        Total += 2;
        debug(s);
    }
}

// Find the objects in the linkset with the names 'left; and 'right'
find_pages() {
    integer i = llGetNumberOfPrims();
    for (; i >= 0; --i) {
        string name = llGetLinkName(i);
        if (name == "left") {
            LeftLink = i;
        }
        else if (name == "right") {
            RightLink = i;
        }
    }
}

reset() {
    // Tear down old stuffs
    llListenRemove(hChannel);

    // Start anew
    hChannel = llListen(CHANNEL, "", NULL_KEY, "");

    find_pages();
    load_pages();
    llMessageLinked(LINK_ALL_OTHERS, -1, "reset", "");
}

default {
    state_entry() {
        reset();
        llSetTimerEvent(time);

        // Start with the first page
        next();
    }


    link_message(integer send_num, integer num, string msg, key id) {
        debug("rcv: " + (string)num + ": " + msg);
        if (num == 1 &&  msg == "left") {
            // Page left
            prev();
        }
        else if (num == 1 &&  msg == "right") {
            // Page right
            next();
        }
    }

    touch_start(integer total_number) {
        integer link = llDetectedLinkNumber(0);
        string name = llGetLinkName(link);
        vector p = llDetectedTouchST(0);

        // handle 2 buttons
        integer bn = (integer)(p.x * 2);

        debug("select: "+name+" "+(string)bn);
        // Check for named prim first, it can be form any location on the prim
        if (name == "left" ) {
            // Page left
            prev();
        }
        else if (name == "right") {
            // Page right
            next();
        }
        // Now look for clicks on the root prim, which is divided into left and right sides
        else if (bn == 0) {
            // Page left
            prev();
        }
        else if (bn == 1) {
            // Page right
            next();
        }
    }

    listen(integer channel, string name, key id, string msg) {
        integer num = (integer)msg;

        if (msg == "reset") {
            reset();
        }
        else if (msg == "debug") {
            llMessageLinked(LINK_ALL_OTHERS, -1, "debug", "");
            VERBOSE = !VERBOSE;
        }
        else if (msg == "left") {
            // Page left
            prev();
        }
        else if (msg == "right") {
            // Page right
            next();
        }
    }

    timer() {
        next();
    }

    on_rez(integer start_param) {
        llResetScript();
    }

    changed(integer change) {
        if (change & CHANGED_INVENTORY) {
            load_pages();
            show();
        }
    }
}
