// diamond particles
//
// Commands:
// on
// off
// toggle
// show
// hide

// Listen channel
integer CHANNEL = 40;

integer particle_state = 0;

// Set num_links = 0 to detect and loop through the linkset
// or use one of the link constants: LINK_SET, LIKN_ROOT, LINK_THIS
integer num_links = LINK_THIS;

integer VERBOSE = FALSE;
log(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}

particles(integer link) {
        llLinkParticleSystem(link, [  // start of particle settings
            PSYS_SRC_PATTERN,PSYS_SRC_PATTERN_ANGLE_CONE,
            PSYS_SRC_BURST_RADIUS,12,
            PSYS_SRC_ANGLE_BEGIN,0.5,
            PSYS_SRC_ANGLE_END,0,
            PSYS_SRC_TARGET_KEY,llGetKey(),
            PSYS_PART_START_COLOR,<1.000000,1.000000,1.000000>,
            PSYS_PART_END_COLOR,<1.000000,1.000000,1.000000>,
            PSYS_PART_START_ALPHA,1,
            PSYS_PART_END_ALPHA,1,
            PSYS_PART_START_GLOW,0.3,
            PSYS_PART_END_GLOW,0.2,
            PSYS_PART_BLEND_FUNC_SOURCE,PSYS_PART_BF_SOURCE_ALPHA,
            PSYS_PART_BLEND_FUNC_DEST,PSYS_PART_BF_ONE_MINUS_SOURCE_ALPHA,
            PSYS_PART_START_SCALE,<0.050000,0.050000,0.000000>,
            PSYS_PART_END_SCALE,<0.200000,0.200000,0.000000>,
            PSYS_SRC_TEXTURE,"05823901-e39c-318d-bd74-2b9c14933772",
            PSYS_SRC_MAX_AGE,0,
            PSYS_PART_MAX_AGE,15,
            PSYS_SRC_BURST_RATE,0.6,
            PSYS_SRC_BURST_PART_COUNT,10,
            PSYS_SRC_ACCEL,<0.000000,0.000000,-0.200000>,
            PSYS_SRC_OMEGA,<0.000000,0.000000,0.000000>,
            PSYS_SRC_BURST_SPEED_MIN,0.3,
            PSYS_SRC_BURST_SPEED_MAX,0.7,
            PSYS_PART_FLAGS,
                0 |
                PSYS_PART_INTERP_SCALE_MASK
        ]);
}

particles_off() {
    log("particles off");
    particle_state = -1;
    if (num_links < 0) {
        // a constant has been specified
        llLinkParticleSystem(num_links, []);
    } else {
        integer i;
        for(i=0; i <= num_links; ++i) {
            log("link: "+(string)i);
            llLinkParticleSystem(i, []);
        }
    }
}

particles_on() {
    log("particles on");
    particle_state = 1;
    if (num_links < 0) {
        // a constant has been specified
            log("link: "+(string)num_links);
        particles(num_links);
    } else {
        integer i;
        for(i=0; i <= num_links; ++i) {
            log("link: "+(string)i);
            particles(i);
        }
    }
}

toggle() {
    if (particle_state > 0) {
        particles_off();
    } else {
        particles_on();
    }
}

hide() {
    llSetAlpha(0.0, ALL_SIDES);
}

show() {
    llSetAlpha(1.0, ALL_SIDES);
}

reset() {
    if (num_links >= 0) {
        num_links = llGetObjectPrimCount(llGetKey());
    }
//    if (num_links < 1) num_links = 1;
    log("reset(): " + (string)num_links);
    particles_off();
//    hide();
}

default {
    state_entry() {
        reset();

        llListen(CHANNEL, "", NULL_KEY, "");
        log("listening on " + (string)CHANNEL);
    }

    listen(integer channel, string name, key id, string message) {
        // Parse command
        string cmd = "";
        string args = "";
        integer idx = llSubStringIndex(message, " ");
        if (idx == -1) {
            cmd = message;
            args = "";
        } else {
            cmd = llGetSubString(message, 0, idx-1);
            args = llDeleteSubString(message, 0, idx);
        }
        cmd = llToLower(cmd);
        log("cmd: " + cmd);

        // Parse a number too?
        integer num = (integer)cmd;

        if (cmd == "reset") {
            reset();
        } else if (cmd == "off") {
            particles_off();
        } else if (cmd == "on") {
            particles_on();
        } else if (cmd == "toggle") {
            toggle();
        } else if (cmd == "show") {
            show();
        } else if (cmd == "hide") {
            hide();
        }
    }

    changed(integer change) {
        if (change & CHANGED_LINK){
            reset();
        }
    }
}
