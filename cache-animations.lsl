// cache animations
// SPDX-License-Identifier: MIT
// v1 27Jul2020 Layne Thomas <tomjs@avimail.org>
//
// Quickly run through all animations in inventory to encourage servers
// and viewers to pre-cache them.
// 
// - Put this script into an object
// - Add some animations to the object inventory
// - Set the object description to set the hover text
// - Decorate the object to make it pleasing to look at (optional)
// - Sit on the object
// - Each animation in inventory will be played for a short time (set TIME below)

// How long to play each animation
float TIME = 0.5;

// Set the starting position and rotation of the sit target
vector sit_pos = <0.00, 0.00, 0.91>;
rotation sit_rot = ZERO_ROTATION;

// Fiddle with vertical position toggle
integer ADJUST = FALSE;

// Set this for the right-click pie menu text
string SIT_TEXT = "Cache";

// Use memory wisely
integer MEM_LIMIT = 12000;

// Global state vars
string current_anim;
key current_avi = NULL_KEY;
integer orig_num_links;

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);

        llSetLinkPrimitiveParamsFast(LINK_THIS, [
            PRIM_TEXT, llGetObjectDesc(), <1,1,1>, 1
        ]);

        // Set the sit target in all linked prim
        integer i = llGetNumberOfPrims();
        if (i == 1) {
            llLinkSitTarget(LINK_THIS, sit_pos, sit_rot);
        } else {
            do {
                llLinkSitTarget(i, sit_pos, sit_rot);
            } while (--i);
        }

        // Set the right-click sit text
        if (llStringLength(SIT_TEXT) > 0)
            llSetSitText(SIT_TEXT);
        llSetClickAction(CLICK_ACTION_SIT);
        orig_num_links = llGetNumberOfPrims();
//        llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
    }

    changed (integer change) {
        if (change & CHANGED_LINK) {
            integer link = llGetNumberOfPrims();
            if (link > orig_num_links) {
                // Link count goes up when an avi sits
                // The last link is the avi
                key avi_key = llGetLinkKey(link);
                if (avi_key != NULL_KEY) {
                    // Save who sat down for later
                    current_avi = avi_key;
                    if (avi_key == llAvatarOnLinkSitTarget(link)) {
                        // We need to make the position and rotation local to the current prim
                        list local;
                        if (llGetLinkKey(link) != llGetLinkKey(1)) {
                            // Need the local rot if it's not the root.
                            local = llGetLinkPrimitiveParams(link, [PRIM_POS_LOCAL, PRIM_ROT_LOCAL]);
                        }
                        vector size = llGetAgentSize(avi_key);
                        float fAdjust = ((((0.008906 * size.z) + -0.049831) * size.z) + 0.088967) * size.z;
                        vector new_pos = sit_pos + <0.00, 0.00, (size.z / 2)>;
                        if (ADJUST) {
                            new_pos = ((new_pos - (llRot2Up(sit_rot) * fAdjust)) * llList2Rot(local, 1));
                        }
                        llSetLinkPrimitiveParamsFast(link, [
                            PRIM_POS_LOCAL, new_pos + llList2Vector(local, 0),
                            PRIM_ROT_LOCAL, sit_rot * llList2Rot(local, 1)
                        ]);
                    }
                    llRequestPermissions(current_avi, PERMISSION_TRIGGER_ANIMATION);
                }
                else if (current_avi != NULL_KEY && avi_key == NULL_KEY) {
                    // someone standing up
                    llStartAnimation("stand");
                    llStopAnimation(current_anim);
                    current_avi = NULL_KEY;
                    current_anim = "";
                }
                else if (current_avi != NULL_KEY && avi_key != current_avi) {
                    // someone sitting but object already occupied by someone else
                    llUnSit(avi_key);
                    llRegionSayTo(avi_key, 0, "Sorry, someone else is already sitting here");
                }
            }
            orig_num_links = llGetNumberOfPrims();
        }
        else if (change & CHANGED_INVENTORY) {
            llSetLinkPrimitiveParamsFast(LINK_THIS, [
                PRIM_TEXT, llGetObjectDesc(), <1,1,1>, 1
            ]);
        }
    }

    run_time_permissions(integer perm) {
        if (perm & PERMISSION_TRIGGER_ANIMATION) {
            llStartAnimation("stand");
            llStopAnimation("sit");
            integer num = llGetInventoryNumber(INVENTORY_ANIMATION);
            integer i = 0;
            while (i < num) {
                current_anim = llGetInventoryName(INVENTORY_ANIMATION, i);
                llStartAnimation(current_anim);
                llSleep(TIME);
                llStartAnimation("stand");
                llStopAnimation(current_anim);
                i++;
            }
            llUnSit(current_avi);
        }
    }
}
