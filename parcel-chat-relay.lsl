// parcel-chat-relay - Relay script for local chat
// From https://wiki.secondlife.com/wiki/ParcelChatRelay
//
// lt3 - Configure in Description
// lt4 - Fix showing Display Names with 2 or 4 byte characters
//
// The lt releases of PCR are based on PCR Version 2 in the Second Life Wiki as of 05 Jan 2016.
// 2011-2012 Tano Toll - TSL License
// Modifications by Layne Thomas are under a traditional BSD license or GPL v3, your choice.

// Relays know of each others existance
// Relays listen to public chat
// The relay _closest_ to sending agent is responsible for relaying the message
// Messages will only be relayed to agents further than 20 meter away from the speaker
// Messages will only be relayed to agents on the same parcel

// USAGE
// * Edit coverage parameter below to suit your needs
// * place this script in a objects, placed in hearing range of avatars
// * If you do NOT want to relay a certain place, like a skybox, simply place no relay object.
//   However, agents in the skybox will still hear what is said near other relays. If you do not
//   want that, please use version 1 of this script that uses radar to detect agents.

// CONFIGURATION

// Choose coverage:
//integer coverage=AGENT_LIST_PARCEL;
integer coverage=AGENT_LIST_PARCEL_OWNER;
//integer coverage=AGENT_LIST_REGION;

// Maximum range from listener object to avatar
//integer maxRange=6000; //sim wide, reaches all skyboxes
integer maxRange=290; //sim wide, ground level
//integer maxRange=100; //radar range, like version 1

// Set a unique RELAY_NET_ID for each group of relays in the same parcel
string RELAY_NET_ID = "+-/";

// NO NEED TO EDIT BELOW THIS LINE. CHANGES AT OWN RISK //

//list of detected nearby agents:
list insim;

//timestamp of last check - to reduce sim load
integer stamp;

//key of the parcel where we are rezzed
key parcelkey;

//key based for handle, depending on setting parcel or owner
key handlebase; 

//chat channel handle made with key as base
integer handle;

//list of detected nearby relays
list repeaters;

//for less confusing setup, fast timer first minutes after rez, then slow down to appropiate interval
integer timercount;

// Our original object name
string obj_name;

// Memory limit
integer MEM_LIMIT = 32000;

integer VERBOSE = FALSE;

// Sets globals RELAY_NET_ID, VERBOSE
parse_args(string desc) {
    list _args = llParseStringKeepNulls(desc, [ ":" ], []);

    integer i = 0;
    string s = "";
    for (; i < llGetListLength(_args); ++i) {
        s = llList2String(_args, i);
        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);

        if (_key == "id") {
            // Set channel
            RELAY_NET_ID = _val;
        }
        else if (_key == "range") {
            // Set channel
            maxRange = (integer)_val;
        }
        else if (_key == "verbose") {
            // Turn on noise
            VERBOSE = TRUE;
        }
    }
}

log(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}

announce() {
    llRegionSay(handle, RELAY_NET_ID + (string)handlebase);
}

// Determine if any characters in the input string are Unicode/UTF-16
integer hazUnicode(string input) {
    integer result = 0;
    integer n = llStringLength(input);
    while (~--n) {
        result = result | (llOrd(input, n) > 255);
    }
    return result;
}

default {
    state_entry() {
        // Set up memory constraints
        llSetMemoryLimit(MEM_LIMIT);

        // Get config from Description
        parse_args(llGetObjectDesc());
        log("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());

        llOwnerSay("Chat Relay started...");
        if (!llGetAttached()) {
            //relay all agents
            llListen(0, "", "", "");
        }

        //make up a inter-relay channel, so they can find eachother
        parcelkey=llList2Key(llGetParcelDetails(llGetPos(), [PARCEL_DETAILS_ID]),0);

        //adjust key based on coverage. for single parcel, use parcel key. else use owner key.
        //notice that on region-wide coverage, all relays must be rezzed by same owner.

        if (coverage==AGENT_LIST_PARCEL)
            handlebase=parcelkey;
        else
            handlebase=llGetOwner();

        handle=-10909-(integer)("0x"+llGetSubString(handlebase,1,5));
        llListen(handle, "", "", RELAY_NET_ID + (string)handlebase);

        obj_name = llGetObjectName();

        //broadcast our existance about every 5 minutes, but starting with 10 second interval to avoid setup confusion
        llSetTimerEvent(10.0);
        announce();
        log("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
    }

    on_rez(integer n) {
        //simply reset
        llResetScript();
    }

    timer() {
        //announce self
        announce();

        //check interval
        timercount++;
        if (timercount==60) //after about 10 minutes
            llSetTimerEvent(290.0+llFrand(20.0)); //change to 5 minute interval

        //and clear list. repeaters with an too old timestamp will be removed.
        integer n=llGetListLength(repeaters)-1;
        integer t=llGetUnixTime();
        while (n>0) {
            if ((t-llList2Integer(repeaters, n))>390) { //time out
                llOwnerSay("Lost connection to node at "+(string)llList2Vector(repeaters, n-1));
                repeaters=llDeleteSubList(repeaters, n-2, n);
            }
            n-=3;
        }
    }

    listen(integer channel, string name, key id, string m) {
        if (!channel) { //channel 0
            //only relay agents, not objects
            if (llGetAgentSize(id)); else
                return;

            //see if another relay is more nearby
            integer n=0;
            vector senderpos=llList2Vector(llGetObjectDetails(id, [OBJECT_POS]), 0);
            float d=llVecDist(senderpos, llGetPos());
            while (n<llGetListLength(repeaters)) {
                if (d>llVecDist(senderpos, llList2Vector(repeaters, n+1)))
                    return;
                n+=3;
            }

            //We are nearest repeater. Our job to relay.

            //see if we need to refresh our list - cache it for 15 seconds
            if ((llGetUnixTime()-stamp) > 15.0) {
                stamp=llGetUnixTime();
                insim=llGetAgentList(coverage, []);
            }

            //relay to everyone in range. that's fixed to minimum 20 mins maximum 60.
            //get position of chatter
            vector o=llList2Vector(llGetObjectDetails(id, [OBJECT_POS]), 0);

            n=llGetListLength(insim);

            name = llGetDisplayName(id);
            string name = name + " (" + llGetUsername(id) + "):";
            if (VERBOSE) {
                name = "[" + obj_name + "] " + name;
            }

            // Display names with 2 or 4 byte chars (UTF-16 or Unicode) display
            // poorly, if at all, when passed to llSetObjectName() so we will
            // fake it and live with the slight difference in look in Local Chat
            if (hazUnicode(name)) {
                llSetObjectName("");
                m = name + " " + m;
            } else {
                llSetObjectName(name);
            }
            while (~--n) {
                vector t=llList2Vector(llGetObjectDetails(llList2Key(insim, n), [OBJECT_POS]), 0);
                float d=llVecDist(o,t);
                if (d>=20 && d<=maxRange) {
                    llRegionSayTo(llList2Key(insim,n), 0, m);
                }
            }
            llSetObjectName(obj_name);
        }
        else if (channel==handle) {
            //there's another repeater..
            vector pos=llList2Vector(llGetObjectDetails(id, [OBJECT_POS]),0);

            integer p=llListFindList(repeaters, [id, pos]);
            if (~p)
                repeaters=llDeleteSubList(repeaters, p, p+2);
            else
                llOwnerSay("New node found at "+(string)pos);
            repeaters += [id, pos, llGetUnixTime()];
        }
    }
}
