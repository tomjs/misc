// proximity_sensor
// activate an object via sensor scan or touch

// v1 (modified from activate_with_touch_or_scan_3_with_report)

// Configure the following in the object description field:
// <avatar>:[alpha]:[light]:[touch]:[range=<float>]
// <avatar> - an avatar 'legacy name' (ie, 'tomjs Resident')    sensor_name
// alpha - enables alpha changing function if present           do_ALPHA
// light - enables light setting function if present            do_LIGHT
// touch - enables touch to activate                            allow_TOUCH
// range=<float> - sets the sensor range  (default is 2.0m)     range

// After changing the description filed it may be necessary to touch the prim
// for it to pick up any changes.  Resetting the script will also work.

// Set to trigger on a single avatar
// This is used as the "name" argument to llSensor, so may be an avatar
// name (legacy name), ie 'tomjs Resident'
string sensor_name = "";

// Set to twiddle alpha
integer do_ALPHA = FALSE;

// Set to twiddle point light settings
integer do_LIGHT = FALSE;

// Set to also allow activation by touch
integer allow_TOUCH = FALSE;

// Set alpha values
float active_alpha = 1.0;
float inactive_alpha = 0.5;

// Light setings
vector color = <1.0, 1.0, 1.0>;
float intensity = 1.000;          // Use to change the intensity of the light, from 0 up to 1
float radius = 10.000;            // Use to change the Radius of the light, from 0 up to 20
float falloff = 0.750;            // Use to set the falloff of the light, from 0 up to 2
integer light_side = ALL_SIDES;   // Use to select the side to light, from 0 up to N-1

// Just this prim or all?
integer link = LINK_THIS;
//integer link = LINK_SET;


// Sensor type mask
integer sensor_type = AGENT;

// Scan range
float range = 2.0;

// Scan frequency
float freq = 1.0;

// Scan angle
float arc = PI;


// TRUE if light is on
integer is_active = FALSE;

// TRUE if turned on by touch
integer force_active;

// Memory
integer mem_limit = 20000;

integer VERBOSE = FALSE;

debug(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}

// Get the sensor target from the arg list
// <avatar>:[alpha]:[light]:[touch]:[range=<float>]
parse_args(string desc) {
    list _args = llParseStringKeepNulls(desc, [ ":" ], []);

    string _name = llList2String(_args, 0);
    if (_name != "") {
        sensor_name = _name;
    }

    integer i = 1;
    string s = "";
    s = llList2String(_args, i);
    while (s != "") {
        debug("arg: " + s);

        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);

        if (_key == "range") {
            range = (float)_val;
        }
        else if (s == "alpha") {
            do_ALPHA = TRUE;
        }
        else if (s == "noalpha") {
            do_ALPHA = FALSE;
        }
        else if (s == "light") {
            do_LIGHT = TRUE;
        }
        else if (s == "nolight") {
            do_LIGHT = FALSE;
        }
        else if (s == "touch") {
            allow_TOUCH = TRUE;
        }
        else if (s == "notouch") {
            allow_TOUCH = FALSE;
        }
        else if (s == "debug") {
            VERBOSE = TRUE;
        }
        i++;
        s = llList2String(_args, i);
    }
}

deactivate() {
    is_active = FALSE;
    if (do_ALPHA) {
        llSetText("", <1,1,1>, 1);
        llSetLinkAlpha(link, inactive_alpha, ALL_SIDES);
    }
    set_light_params();
}

activate() {
    is_active = TRUE;
    if (do_ALPHA) {
        llSetText("", <1,1,1>, 1);
        llSetLinkAlpha(link, active_alpha, ALL_SIDES);
    }
    set_light_params();
}

set_light_params() {
    if (do_LIGHT) {
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_POINT_LIGHT, is_active, color, intensity, radius, falloff]);
        llSetLinkPrimitiveParamsFast(LINK_THIS, [PRIM_FULLBRIGHT, light_side, is_active]);
    }
}

reset() {
    llResetScript();

    // Set up memory constraints
    llSetMemoryLimit(mem_limit);
    llScriptProfiler(PROFILE_SCRIPT_MEMORY);
}

default {
    on_rez(integer param) {
        reset();
    }

    state_entry() {
        reset();
        if (do_LIGHT || do_ALPHA) {
            llSetTimerEvent(freq);

            if (is_active == TRUE) {
                state Active;
            } else  {
                is_active = FALSE;
                state Inactive;
            }
        } else {
            // nothing enabled, idle
            llSetTimerEvent(0.0);
            return;
        }
    }
}


// ACTIVE state
state Active {
    touch_start(integer total_number) {
        parse_args(llGetObjectDesc());
        if (allow_TOUCH) {
            force_active = FALSE;
            deactivate();
            state Inactive;
        }
        llScriptProfiler(PROFILE_NONE);
        debug(
            "\nMax memory: " + (string)llGetSPMaxMemory() +
            "\nUsed memory " + (string)llGetUsedMemory() +
            "\nFree memory " + (string)llGetFreeMemory()
        );
    }

    no_sensor() {
        if (force_active == FALSE) {
            deactivate();
            state Inactive;
        }
    }

    timer() {
        llSensor(sensor_name, NULL_KEY, sensor_type, range, arc);
    }
}


// INACTIVE state
state Inactive {
    touch_start(integer total_number) {
        parse_args(llGetObjectDesc());
        if (allow_TOUCH) {
            force_active = TRUE;
            activate();
            state Active;
        }
        llScriptProfiler(PROFILE_NONE);
        debug(
            "\nMax memory: " + (string)llGetSPMaxMemory() +
            "\nUsed memory " + (string)llGetUsedMemory() +
            "\nFree memory " + (string)llGetFreeMemory()
        );
    }

    sensor(integer iDetected) {
        activate();
        state Active;
    }

    timer() {
        llSensor(sensor_name, NULL_KEY, sensor_type, range, arc);
    }
}
