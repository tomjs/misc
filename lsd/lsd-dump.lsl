// lsd-dump - Tool for looking at linkset_data
//
// Debugging tool for LSD use, plus test some expected behaviours
//
// Long-touch (2+ sec) to get the popup menu

list POPUP_OPTIONS = ["Dump"];
string POPUP_TEXT = "lsd-dump";
integer LISTEN_CHANNEL = -1;
float MENU_TIMEOUT = 30.0;
integer listen_handle;

// Show some stats about the contents of this object's LSD
ls_data_stats() {
    llOwnerSay("LS data available: " + (string)llLinksetDataAvailable());
    llOwnerSay("LS num keys: " + (string)llLinksetDataCountKeys() + "\n");
}

// Dump the contents of all LSD keys
ls_data_dump() {
    list keys = llLinksetDataListKeys(0, -1);
    integer count = llLinksetDataCountKeys();
    if (count != llGetListLength(keys)) {
        llOwnerSay("ERROR: llLinksetDataCountKeys() does not equal the number of keys returned by llLinksetDataListKeys()");
    }
    
    integer i = 0;
    for (i=0; i<count; ++i) {
        llOwnerSay(llList2String(keys, i) + "=" + llLinksetDataRead(llList2String(keys, i)));
    }
}

search_ls_data(string pattern) {
    list keys = llLinksetDataFindKeys(pattern, 0, -1);
    integer count = llGetListLength(keys);

    llOwnerSay("LS num keys found: " + (string)count);
    
    integer i = 0;
    for (i=0; i<count; ++i) {
        llOwnerSay(llList2String(keys, i) + "=" + llLinksetDataRead(llList2String(keys, i)));
    }
}

default {
    state_entry() {
    }

    touch_start(integer total_number) {
        llResetTime();
    }

    touch_end(integer num) {
        if (llGetTime() > 2.0) {
            listen_handle = llListen(LISTEN_CHANNEL, "", NULL_KEY, "");
            llDialog(llDetectedKey(0), POPUP_TEXT, POPUP_OPTIONS, LISTEN_CHANNEL);
            llSetTimerEvent(MENU_TIMEOUT);
        }
    }

    listen(integer channel, string name, key id, string message) {
        if (channel == LISTEN_CHANNEL) {
            llListenRemove(listen_handle);
            if (message == "Dump") {
                ls_data_stats();
                ls_data_dump();
            }
            else if (message == "Off") {
                // Off
            }
            else if (message == "On") {
                // On
            }
        }
    }

    timer(){
        llListenRemove(listen_handle);
        llSetTimerEvent(0);
    }
}
