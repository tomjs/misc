// no-particles - Reset particle system on all links in object
// self-deleting

default {
    state_entry() {
        // Reset particles
        integer num_links = llGetNumberOfPrims();
        integer i;
        for(i=0; i <= num_links; ++i) {
            llLinkParticleSystem(i, []);
        }

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
