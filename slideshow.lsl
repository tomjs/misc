// slideshow - Cycle through textures in inventory
//
// v1 - Initial version with configuration
// v2 - Add pre-caching
// v3 - Add select-by-name
// v4 - Add setting texture, color, alpha directly
// v5 - Add multiple-surface dissolves
// v6 - more + set_size()
// v7 - Add cache command
// v8 - Fix cache faces, add start and stop commands
// v9 - Fade both faces to let alphas in the textures pass through properly
//
// hide                         - hide the display
// show                         - show the display
// next                         - show the next texture from inventory
// prev                         - show the previous texture from inventory
// set <name-or-number>         - display a specific texture by name or number in inventory
// start [<name-or-number>]     - start a timed slideshow with the specified texture
// stop                         - stop a timed slideshow
// <name-or-number>             - display a specific texture by name or number in inventory
//
// * Click on the image to cycle through textures in inventory, click on the
//   left side to go back and on the right side to go forward. (ALLOW_CLICKS=TRUE)
// * Slideshow mode to cycle through inventory textures (DEFAULT_TIME > 0)
// * Send 'next' and 'prev' commands via 'channel' in local chat to change the
//   texture.
// * Display prim get out of the way when hidden (HIDDEN_SIZE and VISIBLE_SIZE)
// * Configure via object description (separate arguments with colons (':')
//   * Set 'channel' to a positive number to listen for commands in local chat
//     channel=55
//   * Set 'time' to auto-advance through the textures (in seconds)
//     time=5
//   * Set 'face' to select the prim face to display (default=all)
//     face=2
//   * Set 'cache' to pre-cache textures on specific face of the root prim (default ALL_SIDES)
//     cache=5
//   * Set 'float' or 'nofloat' (default) to display the texture name as floating
//     text above the prim.
//   * Set 'click' (default) or 'noclick' to allow or ignore clicking on the
//     display to change the texture.
//     click
//   * Set 'inv' to allow anyone to add textures via drag/drop (default FALSE)
//     inv
//   * Example:
//     channel=55:face=2:noclick
//
// If LINK_BACK is not 0, we switch to a mode to dissolve between two prim faces
// when changing textures. It is assumed that this script is in the root prim and
// the 'front' prim face (LINK_FRONT, FACE_FRONT) that will do the dissolving. The
// second prim link number must be set in LINK_BACK.  If the object names 'front' and
// 'back' are set on the display prims they will be auto-detected.

// Set to image advance time, 0 for no auto-advance
float DEFAULT_TIME = 0;

// Default listen channel
integer DEFAULT_CHANNEL = 3090;

// Set to the size of the display faces
// The X and Z values are the display face size
vector HIDDEN_SIZE = <0.01, 0.01, 0.01>;
vector VISIBLE_SIZE = <16.00, 0.05, 9.00>;

// Dissolve
// Dissolve works by using two identical prim with the display faces set about 0.01m apart.
// LINK_FRONT identifies the front prim, LINK_BACK identifies the rear prim.

integer LINK_FRONT = 2;
integer FACE_FRONT = 1;
integer OTHER_FACE_FRONT = 3;

// Set LINK_BACK to 0 to disable dissolve
integer LINK_BACK = 3;
integer FACE_BACK = 1;
integer OTHER_FACE_BACK = 3;

float DISSOLVE_TIME = 1.0;
integer DISSOLVE_STEPS = 10;

// Set to use anything other than full opaque and full transparent
float MIN_ALPHA = 0.0;
float MAX_ALPHA = 1.0;

// Alpha value for non-display faces
float DEFAULT_ALPHA = 0.5;

// Is the front face visible?
integer front_state = 1;

float display_alpha = MAX_ALPHA;

// Display floating text
integer SHOW_FLOAT = FALSE;

// Hide on rez
integer HIDE_ON_REZ = TRUE;
integer hidden = HIDE_ON_REZ;

// Memory
integer MEM_LIMIT = 32000;

// Allow clicking on image to change
integer ALLOW_CLICKS = FALSE;

// Allow inventory drop by all users
integer ALLOW_INV = FALSE;

// Spew debug info
integer VERBOSE = FALSE;

integer num_textures;
integer counter = -1;

float DISPLAY_TIME;
integer CHANNEL;
integer FACE_CACHE = 0;

debug(string msg) {
    if (VERBOSE) {
        llOwnerSay(msg);
    }
}

// Parses the object description into globals
// <time>:<channel>:<face>[:float|:nofloat][:click|:noclick][:inv][:debug]
// All values are set directly and not returned

parse_desc(string desc) {
    list _args = llParseString2List(desc, [ ":" ], []);

    // Set defaults
    DISPLAY_TIME = DEFAULT_TIME;
    CHANNEL = DEFAULT_CHANNEL;

    integer i = 0;
    string s = llList2String(_args, i);
    while (s != "") {
        debug("arg: " + s);

        // Look for key=value args
        list kv = llParseString2List(s, ["="], []);
        string _key = llList2String(kv, 0);
        string _val = llList2String(kv, 1);
        if (_key == "time") {
            DISPLAY_TIME = (float)_val;
        }
        else if (_key == "channel") {
            CHANNEL = (integer)_val;
        }
        else if (_key == "face") {
            FACE_FRONT = (integer)_val;
            FACE_BACK = FACE_FRONT;
        }
        else if (_key == "cache") {
            FACE_CACHE = (integer)_val;
        }

        // Look for boolean args
        else if (s == "float") {
            SHOW_FLOAT = TRUE;
        }
        else if (s == "nofloat") {
            SHOW_FLOAT = FALSE;
        }
        else if (s == "click") {
            ALLOW_CLICKS = TRUE;
        }
        else if (s == "noclick") {
            ALLOW_CLICKS = FALSE;
        }
        else if (s == "inv") {
            ALLOW_INV = TRUE;
        }
        else if (s == "debug") {
            VERBOSE = TRUE;
        }
        i++;
        s = llList2String(_args, i);
    }
}

set_size(vector size) {
    // Set non-root panel size

    // Get current position
    list d = llGetLinkPrimitiveParams(LINK_FRONT, [PRIM_POS_LOCAL]);
    vector v = (vector)llList2String(d, 0);
    // Assumes the root prim is at the top of the display prims
    // Resize the prim and move them to the same height (z) as the root
    // Maintain the same y position, assuming this is the separation
    // between the front and back prims
    llSetLinkPrimitiveParamsFast(LINK_FRONT, [
        PRIM_SIZE, size,
        PRIM_POS_LOCAL, <0.0, v.y, -size.z / 2>
    ]);

    // Do both links...

    // Get current position
    d = llGetLinkPrimitiveParams(LINK_BACK, [PRIM_POS_LOCAL]);
    v = (vector)llList2String(d, 0);
    // Assumes the root prim is at the top of the display prims
    // Resize the prim and move them to the same height (z) as the root
    // Maintain the same y position, assuming this is the separation
    // between the front and back prims
    llSetLinkPrimitiveParamsFast(LINK_BACK, [
        PRIM_SIZE, size,
        PRIM_POS_LOCAL, <0.0, v.y, -size.z / 2>
    ]);

}

// DissolveTexture() uses two prim faces to mimic a dissolve.  The two faces
// should be identical and separated by 0.02m or so.
// Assumes LINK_FRONT is the prim with the dissolving face, LINK_BACK is
// the prim with the background face.
DissolveTexture(string texture) {
    debug("DissolveTexture(): "+(string)front_state);
    if (front_state > 0) {
        // If face is visible, set background texture
        llSetLinkTexture(LINK_BACK, texture, FACE_BACK);
        llSetLinkTexture(LINK_BACK, texture, OTHER_FACE_BACK);
        display_alpha = MAX_ALPHA;
    } else {
        // If face is not visible, set texture first
        llSetLinkTexture(LINK_FRONT, texture, FACE_FRONT);
        llSetLinkTexture(LINK_FRONT, texture, OTHER_FACE_FRONT);
        display_alpha = MIN_ALPHA;
    }

    // fade here
    front_state = -front_state;
    float one_step = (1/(float) DISSOLVE_STEPS) * front_state;
    float interval = DISSOLVE_TIME/(float) DISSOLVE_STEPS;
    integer i;
    for ( ; i < DISSOLVE_STEPS; ++i ) {
        llSleep(interval);
        display_alpha += one_step;
        if (! hidden) {
            llSetLinkAlpha(LINK_FRONT, display_alpha, FACE_FRONT);
            llSetLinkAlpha(LINK_FRONT, display_alpha, OTHER_FACE_FRONT);
            llSetLinkAlpha(LINK_BACK, 1-display_alpha, FACE_BACK);
            llSetLinkAlpha(LINK_BACK, 1-display_alpha, OTHER_FACE_BACK);
        }
    }
}

SetCache(integer num) {
    debug("SetCache('" + llGetInventoryName(INVENTORY_TEXTURE, num) + "')");
    llSetTexture(llGetInventoryName(INVENTORY_TEXTURE, num), FACE_CACHE);
}

integer SetCacheName(string name) {
    integer i = 0;
    while (i < num_textures) {
        string texname = llGetInventoryName(INVENTORY_TEXTURE, i);
        if (name == texname) {
            llSetTexture(name, FACE_CACHE);
            return TRUE;
        }
    }
    integer num = (integer)name;
    if (num >=1 && num <= num_textures) {
        debug("num: " + (string)num);
        SetCache(num-1);
        return TRUE;
    }
    return FALSE;
}

SetTexture(integer num) {
    counter = num;
    integer counter_next = num + 1;
    if (counter_next >= num_textures) {
        counter_next = 0;
    }
    if (FACE_CACHE != FACE_FRONT) {
        SetCache(counter_next);
    }
    string texname = llGetInventoryName(INVENTORY_TEXTURE, counter);
    debug("SetTexture('" + texname + "')");
    if (LINK_BACK) {
        DissolveTexture(texname);
    } else {
        llSetTexture(texname, FACE_FRONT);
        llSetTexture(texname, OTHER_FACE_FRONT);
    }
    SetLinkText(LINK_FRONT, texname, <1,1,1>, 1);
}

// Do this rather than directly setting the texture in order to
// keep the counter refreshed.
// Returns success or failure to locate the named texture in inventory.
integer SetTextureName(string name) {
    integer i = 0;
    while (i < num_textures) {
        string texname = llGetInventoryName(INVENTORY_TEXTURE, i);
        if (name == texname) {
            debug("SetTextureName('" + texname + "')");
            if (LINK_BACK) {
                DissolveTexture(name);
            } else {
                llSetTexture(name, FACE_FRONT);
                llSetTexture(name, OTHER_FACE_FRONT);
            }
            counter = i;
            integer counter_next = i + 1;
            if (counter_next >= num_textures) {
                counter_next = 0;
            }
            if (FACE_CACHE != FACE_FRONT) {
                SetCache(counter_next);
            }
            return TRUE;
        }
        i++;
    }
    integer num = (integer)name;
    if (num >=1 && num <= llGetInventoryNumber(INVENTORY_TEXTURE)) {
        debug("num: " + (string)num);
        SetTexture(num-1);
        return TRUE;
    }
    return FALSE;
}

SetLinkText(integer linknum, string text, vector color, float alpha) {
    if (SHOW_FLOAT) {
        llSetLinkPrimitiveParamsFast(linknum, [PRIM_TEXT, text, color, alpha]);
    } else {
        llSetLinkPrimitiveParamsFast(linknum, [PRIM_TEXT, "", color, alpha]);
    }
}

next() {
    counter++;
    if(counter >= num_textures) {
        counter = 0;
    }
    SetTexture(counter);
}

prev() {
    if (counter <= 0) {
        counter = num_textures - 1;
    } else {
        counter--;
    }
    SetTexture(counter);
}

hide(string args) {
    if (LINK_BACK) {
        llSetLinkAlpha(LINK_BACK, 0.0, ALL_SIDES);
    }
    llSetLinkAlpha(LINK_FRONT, 0.0, ALL_SIDES);
    set_size(HIDDEN_SIZE);
    hidden = TRUE;
}

show(string args) {
    set_size(VISIBLE_SIZE);
//    if (LINK_BACK) {
//        llSetLinkAlpha(LINK_BACK, DEFAULT_ALPHA, ALL_SIDES);
//        llSetLinkAlpha(LINK_BACK, MAX_ALPHA, FACE_BACK);
//    }
    llSetLinkAlpha(LINK_FRONT, DEFAULT_ALPHA, ALL_SIDES);
    llSetLinkAlpha(LINK_BACK, DEFAULT_ALPHA, ALL_SIDES);
    llSetLinkAlpha(LINK_FRONT, display_alpha, FACE_FRONT);
    llSetLinkAlpha(LINK_FRONT, display_alpha, OTHER_FACE_FRONT);
    llSetLinkAlpha(LINK_BACK, 1-display_alpha, FACE_BACK);
    llSetLinkAlpha(LINK_BACK, 1-display_alpha, OTHER_FACE_BACK);
    hidden = FALSE;
}

// init() is called from reset() or other times to re-initialize counters and config
init() {
    parse_desc(llList2String(llGetLinkPrimitiveParams(LINK_ROOT, [PRIM_DESC]), 0));
    num_textures = llGetInventoryNumber(INVENTORY_TEXTURE);
}

// reset() is only called at the beginning of the script
reset() {
    integer me = llGetLinkNumber();
    if (me > 0) {
        integer i;
        string x;
        for(i=1; i<=llGetNumberOfPrims();++i) {
            x = llList2String(llGetLinkPrimitiveParams(i, [PRIM_NAME]), 0);
            if (x == "front") {
                LINK_FRONT = i;
            } else if (x == "back") {
                LINK_BACK = i;
            }
        }
    }

    if (ALLOW_INV)
        llAllowInventoryDrop(TRUE);

    init();

    if (HIDE_ON_REZ) {
        hide("");
    } else {
        // Prime both faces with initial image
        llSetLinkTexture(LINK_FRONT, llGetInventoryName(INVENTORY_TEXTURE, 0), FACE_FRONT);
        llSetLinkTexture(LINK_FRONT, llGetInventoryName(INVENTORY_TEXTURE, 0), OTHER_FACE_FRONT);
        llSetLinkTexture(LINK_BACK, llGetInventoryName(INVENTORY_TEXTURE, 0), FACE_BACK);
        llSetLinkTexture(LINK_BACK, llGetInventoryName(INVENTORY_TEXTURE, 0), OTHER_FACE_BACK);
        SetTexture(0);
        show("");
    }
}

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);
        reset();
        llSetTimerEvent(DISPLAY_TIME);

        llListen(CHANNEL, "", NULL_KEY, "");
        debug("listening on " + (string)CHANNEL);
        llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
    }

    on_rez(integer num) {
        debug("on_rez: "+(string)num);
        reset();
    }

    listen(integer channel, string name, key id, string message) {
        // Parse command
        string cmd = "";
        string args = "";
        integer idx = llSubStringIndex(message, " ");
        if (idx == -1) {
            cmd = message;
            args = "";
        } else {
            cmd = llGetSubString(message, 0, idx-1);
            args = llDeleteSubString(message, 0, idx);
        }
        cmd = llToLower(cmd);
        debug("cmd: " + cmd);

        // Parse a number too?
        integer num = (integer)cmd;

        if (cmd == "reset") {
            reset();
        } else if (cmd == "next") {
            next();
        } else if (cmd == "prev") {
            prev();
        } else if (cmd == "hide") {
            hide(args);
        } else if (cmd == "show") {
            show(args);
        } else if (cmd == "cache") {
            SetCacheName(args);
            debug("name: " + args);
        } else if (cmd == "start" || cmd == "set") {
            num = (integer)args;
            if (SetTextureName(args)) {
                // Attempt to set a texture with the passed in name
            } else if (num >=1 && num <= num_textures) {
                SetTexture(num-1);
            }
            if (cmd == "start") {
                llSetTimerEvent(DISPLAY_TIME);
            }
        } else if (cmd == "stop") {
            llSetTimerEvent(0);
        } else if (cmd == "color") {
            // Set color directly
            vector color = (vector)args;
            debug("color="+(string)color);
            if (LINK_BACK) {
                llSetLinkPrimitiveParamsFast(
                    LINK_BACK, [
                    PRIM_COLOR, FACE_BACK, color, 1.0,
                    PRIM_COLOR, OTHER_FACE_BACK, color, 1.0
                ]);
            }
            llSetLinkPrimitiveParamsFast(
                LINK_FRONT, [
                PRIM_COLOR, FACE_FRONT, color, 1.0,
                PRIM_COLOR, OTHER_FACE_FRONT, color, 1.0
            ]);
        } else if (cmd == "alpha") {
            // Set alpha directly
            float alpha = (float)args;
            llSetLinkAlpha(LINK_FRONT, alpha, ALL_SIDES);
        } else if (cmd == "blank") {
            // Set blank texture directly
            if (LINK_BACK) {
                llSetLinkTexture(LINK_BACK, TEXTURE_BLANK, FACE_BACK);
                llSetLinkTexture(LINK_BACK, TEXTURE_BLANK, OTHER_FACE_BACK);
            }
            llSetLinkTexture(LINK_FRONT, TEXTURE_BLANK, FACE_FRONT);
            llSetLinkTexture(LINK_FRONT, TEXTURE_BLANK, OTHER_FACE_FRONT);
        } else if (cmd == "transparent") {
            // Set transparent texture directly
            if (LINK_BACK) {
                llSetLinkTexture(LINK_BACK, TEXTURE_TRANSPARENT, FACE_BACK);
                llSetLinkTexture(LINK_BACK, TEXTURE_TRANSPARENT, OTHER_FACE_BACK);
            }
            llSetLinkTexture(LINK_FRONT, TEXTURE_TRANSPARENT, FACE_FRONT);
            llSetLinkTexture(LINK_FRONT, TEXTURE_TRANSPARENT, OTHER_FACE_FRONT);
        } else if (SetTextureName(message)) {
            // Attempt to set a texture with the passed in name
            debug("name: " + message);
        } else if (num >=1 && num <= num_textures) {
            debug("num: " + (string)num);
            SetTexture(num-1);
        }
    }

    touch_start(integer total_number) {
        llResetTime();
    }

    touch_end(integer num) {
        if (llGetTime() > 2.0) {
            return;
        }

        vector p = llDetectedTouchST(0);
        debug("click detected p: " + (string)p + ", counter: " + (string)counter);

        // handle 2 buttons
        integer bn = (integer)(p.x * 2);
        if (ALLOW_CLICKS && bn == 0) {
            prev();
        }
        else if (ALLOW_CLICKS && bn == 1) {
            next();
        }
    }

    timer() {
        next();
    }

    changed(integer change) {
        if (change & (CHANGED_ALLOWED_DROP | CHANGED_INVENTORY)) {
            // Pick up new textures loaded
            init();
        }
    }
}
