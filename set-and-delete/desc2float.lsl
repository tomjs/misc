// desc2float - Set float text from object description
// self-deleting

// Set to the desired float text, or leave empty to use the object name
// Use '~' to insert a newline
string FLOAT_TEXT = "";

vector color = <1, 1, 1>;
float alpha = 1.0;

// Based on SplitLine() from http://wiki.secondlife.com/wiki/SplitLine
string SplitLine(string _source, string _separator) {
    integer offset = 0;
    integer separatorLen = llStringLength(_separator);
    integer split = -1;

    do {
        split = llSubStringIndex(llGetSubString(_source, offset, -1), _separator);
        llOwnerSay("split="+(string)split+": "+_source);
        if (split != -1) {
            _source = llGetSubString(_source, 0, offset + split - 1) + "\n" + llGetSubString(_source, offset + split + separatorLen, -1);
            offset += split + separatorLen;
        }
    } while (split != -1);
    return _source;
}

default {

    state_entry() {
        if (FLOAT_TEXT == "") {
            // Get description
            FLOAT_TEXT = llGetObjectDesc();
        }

        // Set the floating text
        llSetText(SplitLine(FLOAT_TEXT, "~"), color, alpha);

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
