// simple sit linkset
// Sets up a sit target in each prim of a linkset and starts
// the first animation in object inventory when an avatar sits
// on each link.
//
// v1 - First linkset version with height adjustment
//
// A simple adjustment for avatar height is made to approximate
// standing on the floor.

// Set the starting position and rotation of the sit target
vector sit_pos = <0.00, 0.00, 0.01>;
rotation sit_rot = ZERO_ROTATION;

// Set this for the right-click pie menu text
string SIT_TEXT="Chill";

// Use memory wisely
integer MEM_LIMIT = 10000;

string animation;
integer save_prim_count;

default {
    state_entry() {
        llSetMemoryLimit(MEM_LIMIT);

        // Set the sit target in all linked prim
        integer i = llGetNumberOfPrims();
        do {
            llLinkSitTarget(i, sit_pos, sit_rot);
        } while (--i);

        // Set the right-click sit text
        if (llStringLength(SIT_TEXT) > 0)
            llSetSitText(SIT_TEXT);
        llSetClickAction(CLICK_ACTION_SIT);

        llOwnerSay("Memory: used="+(string)llGetUsedMemory()+" free="+(string)llGetFreeMemory());
    }

    changed(integer change) {
        if (change & CHANGED_LINK) {
            // If link is larger than it was before (save_prim_count) it is the sitting avatar link number
            integer link = llGetNumberOfPrims();
            if (link > save_prim_count) {
                // link is a new sitter
                key av_key = llGetLinkKey(link);
                integer linkNum = link;
                do {
                    if (av_key == llAvatarOnLinkSitTarget(linkNum)) {
                        vector size = llGetAgentSize(av_key);
                        // We need to make the position and rotation local to the current prim
                        list local;
                        if (llGetLinkKey(link) != llGetLinkKey(1))
                            // Need the local rot if it's not the root.
                            local = llGetLinkPrimitiveParams(link, [PRIM_POS_LOCAL, PRIM_ROT_LOCAL]);
                        float fAdjust = ((((0.008906 * size.z) + -0.049831) * size.z) + 0.088967) * size.z;
                        llSetLinkPrimitiveParamsFast(link, [
                            PRIM_POS_LOCAL, ((sit_pos + <0.00, 0.00, (size.z / 2)> - (llRot2Up(sit_rot) * fAdjust)) * llList2Rot(local, 1)) + llList2Vector(local, 0),
                            PRIM_ROT_LOCAL, sit_rot * llList2Rot(local, 1)
                        ]);
                    }
                } while (--linkNum);
                if (av_key) {
                    llRequestPermissions(av_key, PERMISSION_TRIGGER_ANIMATION);
                } else {
                    integer perm = llGetPermissions();
                    if ((perm & PERMISSION_TRIGGER_ANIMATION) && llStringLength(animation) > 0)
                        llStopAnimation(animation);
                    animation = "";
                }
            }
            save_prim_count = llGetNumberOfPrims();
        }
    }

    run_time_permissions(integer perm) {
        if (perm & PERMISSION_TRIGGER_ANIMATION) {
            llStopAnimation("sit");
            animation = llGetInventoryName(INVENTORY_ANIMATION, 0);
            if (llStringLength(animation) > 0) {
                llStartAnimation(animation);
            }
        }
    }
}
