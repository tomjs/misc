# cache animations

This is a very simple animation pre-cache.  When sat on it plays every animation
in its inventory for 0.5 seconds.  This causes viewers in the vicinity to pre-load
those animations so when they are played during a performance they begin
immediately.

* Put this script into an object
* Add some animations to the object inventory
* Set the object description to set the hover text
* Decorate the object to make it pleasing to look at (optional)
* Sit on the object
* Each animation in inventory will be played for a short time (set TIME below)

The advantage of putting them in a stand-alone object like this is when your
 avatars
are on-stage and you need to run the cache without the whole bunch jerking around
wildly.  Put this object offstage and any avatar can sit on it and cause the
animations to be cached.

## Script Adjustments

There are a couple of setting that can be adjusted in the script.  Normally this
should not be necessary.

* ``TIME`` (seconds) - this sets the amount of time to play each animation, default is 0.5 seconds

* ``sit_pos`` (vector) - this sets the sit target . The script makes an attempt to
position the avatar roughly so it is standing at the center line of the object.
Specifically for a box with a height of 0.1 or less it aims for the avatar to
appear to be standing on it.

* ``sit_rot`` (rotation) - this sets the rotation of the sit target.

# Notes

This script licensed under the MIT License, which basically allows anyone to do
almost anything with it except claim it as their own work.  The current version of
the script can be found at https://gitlab.com/tomjs/misc/-/blob/master/cache-animations.lsl

Layne Thomas (tomjs Resident)
laynethomas@avimail.org
