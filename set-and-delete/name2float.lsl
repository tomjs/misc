// name2float - Set float text from object name or string
// self-deleting

// Set to the desired float text, or leave empty to use the object name
string FLOAT_TEXT = "";

vector color = <1, 1, 1>;
float alpha = 1.0;

default {
    state_entry() {
        if (FLOAT_TEXT == "") {
            // Get name
            FLOAT_TEXT = llGetObjectName();
        }

        // Set the floating text
        llSetText(FLOAT_TEXT, color, alpha);

        // Delete me
        llRemoveInventory(llGetScriptName());
    }
}
