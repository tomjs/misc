//// "Jet Flame" PARTICLE TEMPLATE v1 - by Jopsy Pendragon - 4/8/2008
//// You are free to use this script as you please, so long as you include this line:
//** The original 'free' version of this script came from THE PARTICLE LABORATORY. **//

default {
    state_entry() {
        llParticleSystem([  // start of particle settings
           // Texture Parameters:
           PSYS_SRC_TEXTURE, llGetInventoryName(INVENTORY_TEXTURE, 0),
           PSYS_PART_START_SCALE, <1.0, 1.0, FALSE>,  PSYS_PART_END_SCALE, <.0, 2.0, FALSE>,
           PSYS_PART_START_COLOR, <1.0, 0.8, 0.2>,       PSYS_PART_END_COLOR, <0.8, 0.5, 0.2>,
           PSYS_PART_START_ALPHA, (float)1.0,            PSYS_PART_END_ALPHA, (float)0.0,

           // Production Parameters:
           PSYS_SRC_BURST_PART_COUNT, (integer) 1,
           PSYS_SRC_BURST_RATE, (float) 0.01,
           PSYS_PART_MAX_AGE, (float)0.9,
           PSYS_SRC_MAX_AGE,(float) 0.0,

           // Placement Parameters:
           PSYS_SRC_PATTERN, (integer)8, // 1=DROP, 2=EXPLODE, 4=ANGLE, 8=ANGLE_CONE,

           // Placement Parameters (for any non-DROP pattern):
           PSYS_SRC_BURST_SPEED_MIN, (float).01,   PSYS_SRC_BURST_SPEED_MAX, (float)3.01,
           PSYS_SRC_BURST_RADIUS, 0.1,

           // Placement Parameters (only for ANGLE & CONE patterns):
           PSYS_SRC_ANGLE_BEGIN, (float) 0.01*PI,        PSYS_SRC_ANGLE_END,(float) 0.0*PI,
           PSYS_SRC_OMEGA, <0,0,0>,

           // After-Effect & Influence Parameters:
           PSYS_SRC_ACCEL, <0.0,0.0,0.0>,
        // PSYS_SRC_TARGET_KEY,      llGetLinkKey(llGetLinkNumber() + 1),

           PSYS_PART_FLAGS, (integer)( 0           // Texture Options:
                                | PSYS_PART_INTERP_COLOR_MASK
                                | PSYS_PART_INTERP_SCALE_MASK
                                | PSYS_PART_EMISSIVE_MASK
                                | PSYS_PART_FOLLOW_VELOCITY_MASK
                                                  // After-effect & Influence Options:
                             // | PSYS_PART_WIND_MASK
                             // | PSYS_PART_BOUNCE_MASK
                             // | PSYS_PART_FOLLOW_SRC_MASK
                             // | PSYS_PART_TARGET_POS_MASK
                             // | PSYS_PART_TARGET_LINEAR_MASK
                            )
            //end of particle settings
        ]);
    }
}
